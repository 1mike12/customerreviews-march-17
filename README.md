# README #

This is a snapshot of the Clientype project. Clientype is a web application for reviewing households. This project is built in collaboration with Brian Gioia, who concentrates on design and styling. The site is written with Laravel 4.2. Most of the pages are generated through the blade template engine. For highly dynamic content, we use Angularjs and a RESTful api. 

#installation#
1.  set up MySQL database, name "rating"
2. run migrations
3. run database seeders


* Default account (automatically seeded) is admin@piedpiper.com 

* default password 12345

#screenshots#
![splash page.jpg](https://bitbucket.org/repo/e9r6x5/images/1098397177-splash%20page.jpg)
![dashboard.JPG](https://bitbucket.org/repo/e9r6x5/images/3581434681-dashboard.JPG)
![customer_view.JPG](https://bitbucket.org/repo/e9r6x5/images/1039237827-customer_view.JPG)