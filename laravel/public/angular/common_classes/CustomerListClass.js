myApp.factory("CustomerListClass", function ($http, CustomerClass) {

    function Class(CLType) {
        var self = this;
        self.apiBase = "api/";
        self.connection;

        self.ready = false;
        self.customers = [];

        self.init = function () {
            self.connection = self.apiBase + CLType;

            $http.get(self.connection)
                .success(function (payload) {
                    if (payload.status === "empty") {
                        //no items don't extend
                    } else if (payload.status === "success") {
                        angular.forEach(payload.customers, function (my_customer) {
                            var newCust = new CustomerClass(my_customer);
                            newCust.CLType = CLType;
                            self.customers.push(newCust);
                        });
                    }
                    self.ready = true;
                })
                .error(function (payload) {
                    console.log("couldnt access path: " + self.connection + payload.message);
                });
        };

        self.customerCount = function () {
            return self.customers.length;
        };

        self.destroyCustomer = function (customer) {
            self.customers.splice(self.customers.indexOf(customer), 1);

            customer.destroy()
                .success(function (payload) {

                })
                .error(function (payload){
                    console.log("shit broke");
                });
        };

        self.init();
    }

    return Class;
});

