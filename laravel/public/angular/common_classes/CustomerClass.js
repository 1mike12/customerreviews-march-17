myApp.factory("CustomerClass", function ($http) {
    /*extendable object could be :
     {id:1, last_name:"lee", etc
     pivot:{}
     reviews: []
     }
     */
    function Class(extendable) {
        var self = this;
        self.apiBase = "api/";
        self.ready = false;
        self.updated = false;

        self.snapshot = null;
        /*set on instantiation
         ==============
         * self.reviews = [];
         *
         * Assigned after instantiation
         * ====================
         * self.ClType;
         * */

        self.init = function () {
            angular.extend(self, extendable);
        };

        //CRUD- no create (all added from other views) and no retrieve (always in list form)

        //self.create = function () {
        //    var URI = self.apiBase + self.CLType;
        //    $http.post(URI)
        //        .success()
        //        .error();
        //};
        self.retrieve = function () {
            $http.get();
        };

        //auto update when focusing and bluring note text area
        self.focus = function () {
            self.snapshot = self.pivot.note;
        };

        self.blur = function () {
            if (self.pivot.note !== self.snapshot) {
                self.update();
            }
        };
        self.update = function () {
            var URI = self.apiBase + self.CLType + "/" + self.pivot.id;
            $http.patch(URI, {note: self.pivot.note})
                .success(function () {
                    self.updated = true;
                })
                .error(function () {
                });
        };
        self.destroy = function () {
            return $http.delete(self.apiBase + self.CLType + "/" + self.pivot.customer_id);
        };

        self.init();
    }

    return Class;
});