myApp.controller("CustomerListController", function ($attrs, CustomerListClass) {
    var self = this;

    if (!$attrs.controllerInit) throw new Error("Didn't pass a customerListType for CustomerListController");

    self.customerListType= $attrs.controllerInit;
    self.orderReverse = false;
    self.order = "last_name";

    self.CLInstance = new CustomerListClass(self.customerListType);

    self.customerCount = function () {
        return self.CLInstance.customerCount();
    };


    self.changeCompareOrder = function (field) {
        //only toggle orderReverse
        if (field == self.order) {
            self.orderReverse = !self.orderReverse;
        }
        //change field and reset toggle orderReverse
        else {
            self.orderReverse = false;
            self.order = field;
        }
    };

});