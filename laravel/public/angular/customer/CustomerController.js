/**
 * Created by 1mike12 on 3/2/2015.
 */

myApp.controller("CustomerController", function ($attrs, $http, CustomerClass) {

    var self = this;
    self.ready = false;
    //set on init angular.extend
    self.customer = {};
    self.customer_id;
    self.toggleCustomerText = "";
    //will be connection to both /customer and /my_customer
    self.apiBase = "api/";


    self.isMyCustomerLock = false;

    self.console = function () {
        console.log(self);
    };
    self.init = function () {

        self.extendControllerInit();
        if (!self.customer_id) {
            throw new Error("Didn't pass a customer_id for CustomerController");
        }
        var URI = self.apiBase + "customer/" + self.customer_id;
        $http.get(URI)
            .success(function (payload) {
                self.customer = new CustomerClass(payload.customer);
                self.ready = true;

                if (self.customer.isMyCustomer) {
                    self.toggleCustomerText = "My Client";
                } else {
                    self.toggleCustomerText = "Add to Dash";
                }
            })
            .error(function () {

            });
    };

    self.toggleMyCustomer = function () {
        self.toggleCustomerText="Working..";
        if (self.customer.isMyCustomer) {
            self.isMyCustomerLock = true;
            $http.delete(self.apiBase + "my_customers/" + self.customer_id)
                .success(function () {
                    self.customer.isMyCustomer = false;
                    self.isMyCustomerLock = false;
                    self.toggleCustomerText = "Add to Dash";
                })
                .error(function () {

                });
        } else {
            self.isMyCustomerLock = true;
            var URI = self.apiBase + "my_customers";
            $http.post(URI, {'customer_id': self.customer_id})
                .success(function () {
                    self.customer.isMyCustomer = true;
                    self.isMyCustomerLock = false;
                    self.toggleCustomerText = "My Client";

                })
                .error(function () {

                });
        }
    };

    self.extendControllerInit = function () {
        /*
         input format has to be {"param_name": 5,"string",etc}
         */
        var initObj = angular.fromJson($attrs.controllerInit);
        angular.extend(self, initObj);
    };

    self.ratingToClass = function (int) {
        var type;

        if (int >= 3.5) {
            type = "default";
        } else if (int > 1.5) {
            type = "warning";
        } else {
            type = "danger";
        }
        return type;
    };

    self.ratingToPercent = function (int) {
        return Math.round(int / 5 * 100) + "%";
    };
    self.dateParse = function (dateTimeString) {
        var date = new Date(Date.parse(dateTimeString));
        return date.getDate() + "-" + date.getMonth() + "-" + date.getFullYear();
    };
    self.init();
});