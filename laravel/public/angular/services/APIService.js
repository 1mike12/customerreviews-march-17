/**
 * Created by 1mike12 on 2/19/2015.
 */
myApp.factory("APIService", function($http){
    function Service(){
        var self= this;

        self.base="";

        self.init= function(){

        };

        self.getMyCustomers= function(){
            return $http.get("api/my-customers");
        };

        self.init();
    }
    return new Service();
});