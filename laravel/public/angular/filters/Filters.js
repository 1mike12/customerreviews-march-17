/**
 * Created by 1mike12 on 3/4/2015.
 */
myApp.filter("DateTime", function () {
    return function (dateSTR) {
        var o = dateSTR.replace(/-/g, "/"); // Replaces hyphens with slashes
        return Date.parse(o + " -0000"); // No TZ subtraction on this sample
    }
});