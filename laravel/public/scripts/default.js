// Label if you know what this thing does
// ============================================================================================================
var header = $(".home #header");
$window = $(window);

$window.scroll(function () {
    // Get scroll position
    var s = $window.scrollTop();

    if (s > 1) {
        header.removeClass("unScrolled");
    } else if (s < 1) {
        header.addClass("unScrolled");
    }
});

// Initializing Tooltips
// ============================================================================================================
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})

$(function () {
    $('[data-toggle="popover"]').popover().popover('show');
})

// Review Modal - Star Section
// ============================================================================================================
jQuery.fn.ratings = function (stars, initialRating) {

    //Save  the jQuery object for later use.
    var elements = this;

    //Go through each object in the selector and create a ratings control.
    return this.each(function () {

        //Make sure intialRating is set.
        if (!initialRating)
            initialRating = 0;

        //Save the current element for later use.
        var containerElement = this;

        //grab the jQuery object for the current container div
        var container = jQuery(this);

        //Create an array of stars so they can be referenced again.
        var starsCollection = Array();

        //Save the initial rating.
        containerElement.rating = initialRating;


        //create each star
        for (var starIdx = 0; starIdx < stars; starIdx++) {

            //Create a div to hold the star.
            var starElement = document.createElement('i');

            //Get a jQuery object for this star.
            var star = jQuery(starElement);

            //Store the rating that represents this star.
            starElement.rating = starIdx + 1;

            //Add the style.
            star.addClass('jquery-ratings-star');

            //Add the full css class if the star is beneath the initial rating.
            if (starIdx < initialRating) {
                star.addClass('query-ratings-full');
            }

            //add the star to the container
            container.append(star);
            starsCollection.push(star);

            //hook up the click event
            star.click(function () {
                //When clicked, fire the 'ratingchanged' event handler.  Pass the rating through as the data argument.
                elements.triggerHandler("ratingchanged", {rating: this.rating});
                containerElement.rating = this.rating;
            });

            star.mouseenter(function () {
                //Highlight selected stars.
                for (var index = 0; index < this.rating; index++) {
                    starsCollection[index].addClass('jquery-ratings-full');
                }
                //Unhighlight unselected stars.
                for (var index = this.rating; index < stars; index++) {
                    starsCollection[index].removeClass('jquery-ratings-full');
                }
            });

            container.mouseleave(function () {
                //Highlight selected stars.
                for (var index = 0; index < containerElement.rating; index++) {
                    starsCollection[index].addClass('jquery-ratings-full');
                }
                //Unhighlight unselected stars.
                for (var index = containerElement.rating; index < stars; index++) {
                    starsCollection[index].removeClass('jquery-ratings-full');
                }
            });
        }
    });
};


$(document).ready(function () {
    $('#example-1').ratings(5).bind('ratingchanged', function (event, data) {
        var aRatings = ["Terrible", "Bad", "Fair", "Good", "Excellent"];
        $('#example-rating-1').text(aRatings[data.rating - 1]);
    });

    $('#example-2').ratings(5).bind('ratingchanged', function (event, data) {
        var aRatings = ["Terrible", "Bad", "Fair", "Good", "Excellent"];
        $('#example-rating-2').text(aRatings[data.rating - 1]);
    });

    $('#example-3').ratings(5).bind('ratingchanged', function (event, data) {
        var aRatings = ["Terrible", "Bad", "Fair", "Good", "Excellent"];
        $('#example-rating-3').text(aRatings[data.rating - 1]);
    });
});

// Show Current Pennant Link
// ============================================================================================================
jQuery(function () {
    jQuery('a').each(function () {
        if (jQuery(this).attr('href') === window.location.pathname) {
            jQuery(this).addClass('active-link');
        }
    });
});

// Enable Bootstrap Auto Dropdown Nav on Hover
// ============================================================================================================
$('.dropdown-toggle').click(function () {
    var location = $(this).attr('href');
    window.location.href = location;
    return false;
});

// Enable Class on Hover
// ============================================================================================================
$(".pp-icon").hover(
    function () {
        $(this).addClass("fa-spin");
    }, function () {
        $(this).removeClass("fa-spin");
    }
);

// Inisalize carousel data for loop interval
// ============================================================================================================
$('.carousel').carousel({
    interval: 0
});


// Allows active settings tabs
// ============================================================================================================
$(document).ready(function () {
    $('[id^=detail-]').hide();
    $('.toggle').click(function () {
        $input = $(this);
        $target = $('#' + $input.attr('data-toggle'));
        $target.slideToggle();
    });
});

// Client image enlarge on hover
// ============================================================================================================
$(".enlarge").hover(
    function () {
        $(this).addClass("enlarge-save");
    }, function () {
        $(this).removeClass("enlarge-save");
    }
);


// Tab "New Review" fade away
// ============================================================================================================
window.setTimeout(function () {
    $(".tab-seen").fadeTo(500, 0).slideUp(500, function () {
        $(this).remove();
    });
}, 2000);

// Settings Page - Keeps only one tab open at a time
// ============================================================================================================
$('.navbar').on('show.bs.collapse', function () {
    var actives = $(this).find('.collapse.in'),
        hasData;

    if (actives && actives.length) {
        hasData = actives.data('collapse')
        if (hasData && hasData.transitioning) return
        actives.collapse('hide')
        hasData || actives.data('collapse', null)
    }
});


// Call Talk to Users
// ============================================================================================================
!function () {
    window.kivo = window.kivo || {}, kivo.minimized = !1,
        kivo.enableAnnotations = function (a) {
            kivo.enabled = !0,
                kivo.enabledOptions = a
        }, kivo.minimize = function (a) {
        kivo.minimized = a
    },
        window.kivoApiKey = "62505d9573e6";
    var a = document.createElement("script");
    a.type = "text/javascript", a.src = "https://talktousers.com/kivo-talk/dist/browserify.js",
        document.getElementsByTagName("head")[0].appendChild(a)
}();

// call prompt
kivo.enableAnnotations();

// makes it minimized
kivo.minimize(Boolean)


// Call Sweet Alert
// ============================================================================================================
document.getElementById('signout-alert').onclick = function () {
    swal({
        type: "success",
        title: "You are signed out!",
        text: "Thank you",
        confirmButtonColor: "white",
        timer: 1500
    });
};


// Call the dropdowns
// ============================================================================================================
$('.dropdown-toggle').dropdown();

//modal stuff moved from dash.blade.php
$(".active-tag-pro").click(function () {
    $(this).toggleClass("active-tag-pro-after");
});

$(".active-tag-con").click(function () {
    $(this).toggleClass("active-tag-con-after");
});