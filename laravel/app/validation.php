<?php
Validator::extend("domain", function($field, $value, $params) {
    $www = preg_match("/^(www\.).+/", $value);
    $domain = preg_match("/^[a-z0-9-]+\.[a-z]{2,5}(\.[a-z]{2,5})?$/", $value);

    return !$www && $domain;
});

Validator::extend("phone", function($field, $value, $params) {
    $valid = preg_match("/^[0-9-()_\s]+$/", $value);
    $numbers = preg_replace('/\D/', '', $value);
    $longEnough = (strlen($numbers) >= 10 ) ? true : false;
    return $valid && $longEnough;
});