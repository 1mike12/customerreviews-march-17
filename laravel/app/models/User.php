<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

    protected $guarded = ["password", "password_confirmation", "terms"];
    public static $rules = [
        "company_name" => "required|max:128",
        "domain" => "required|unique:users|domain|max:255",
        "address1" => "required|max:255",
        "address2" => "max:255",
        "city" => "required|max:128",
        "state" => "required|max:128",
        "zip" => "required|digits:5|max:16",
        "phone" => "required|phone|unique:users|max:128",
        'email' => 'required|email|unique:users|max:255',
        'password' => 'required|alpha_num|confirmed|max:255',
        'password_confirmation' => 'required|alpha_num',
        "terms" => "accepted"
    ];
    public static $messages = [
        "domain" => "must be of the format example.com or example.sub.dom",
        "phone" => "Must be at least 10 digits, can only contain numbers, spaces, and the following characters: <code>(</code><code>)</code><code>-</code><code>_</code>",
        "email" => "Email should not contain the part after @"
    ];

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');

    public function addressToArray() {
        $address = [];
        $address[] = $this->address1;
        $address[] = $this->address2;
        $address[] = "$this->city, $this->state $this->zip";

        return $address;
    }

    public function recentCustomers() {
        return $this->belongsToMany('Customer', 'recent_customers')
                        ->withTimestamps()
                        ->with("reviews")
                        ->orderBy("created_at");
    }

    //can't name this my_customers because L4 doesn't "do" snake_case
    //withPivot("column_name to include")
    //orderBy() - specifies pivot table's columns
    public function myCustomers() {
        return $this->belongsToMany('Customer', 'my_customers')
                        ->withTimestamps()
                        ->with("reviews")
                        ->withPivot("note", "id")
                        ->orderBy("created_at");
    }

}
