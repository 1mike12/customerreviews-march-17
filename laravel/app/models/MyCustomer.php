<?php

class MyCustomer extends Eloquent {

    //--------------------
    protected $softDelete = true;
    
    public static $rules = [
        "note" => "max:255"
    ];
    
    //can be mass assigned
    protected $fillable = ['note', "customer_id"];
    
    public function customer() {
        return $this->belongsTo("Customer");
    }
}
