<?php

class Customer extends Eloquent {
    //how many ratings a review can have
    const ratingSteps = 5.0;

    //averages generated from Customer->reviews
    //run $this->loadAverages()
    private $averagesLoaded = false;
    public $payAvg;
    public $payPercent;
    public $easeAvg;
    public $easePercent;
    public $niceAvg;
    public $nicePercent;
    public $overallAvg;
    public $overallPercent;
    public $hasReviews = false;
    public $reviewCount;

    //--------------------
    protected $softDelete = true;
    protected $guarded = ["photo"];
    public static $rules = [
        "name" => "required",
        "weight" => "numeric"
    ];

    //custom attributes added to eloquent->toArray()
    //nomenclature- $appends=["x"] & function name getXAttribute()
    protected $appends = ["streetViewURL", "overallAvg", "reviewCount", "addressParsed", "payAvg", "easeAvg", "niceAvg"];

    public function getStreetViewURLAttribute() {
        $this->loadAverages();
        return $this->streetViewURL();
    }

    public function getOverallAvgAttribute() {
        return $this->overallAvg;
    }
    
    public function getPayAvgAttribute(){
        return $this->payAvg;
    }
    
    public function getEaseAvgAttribute(){
        return $this->easeAvg;
    }
    
    public function getNiceAvgAttribute(){
        return $this->niceAvg;
    }

    public function getReviewCountAttribute() {
        return $this->reviewCount;
    }

    public function getAddressParsedAttribute() {
        return $this->addressToString();
    }
    //custom constructor
    public function __construct($attributes = array()) {
        parent::__construct($attributes); // Eloquent
    }

    public function reviews() {
        return $this->hasMany("Review");
    }


    public function streetViewURL($width = "120", $height = "80", $fov = "70") {
        $address = $this->addressToString();
        $url = "http://maps.googleapis.com/maps/api/streetview?size={$width}x{$height}&location=$address&fov=$fov&heading=235&pitch=0";
        return $url;
    }

    public function addressToString() {
        $array = $this->addressToArray();
        $address = implode(", ", $array);
        return $address;
    }

    public function addressToArray() {
        $address = [];
        $address[] = $this->address1;
        if ($this->address2 != "") {
            $address[] = $this->address2;
        }
        $address[] = "$this->city, $this->state $this->zip";

        return $address;
    }

    public function loadAverages() {
        $this->averagesLoaded = true;
        $paySum = 0;
        $easeSum = 0;
        $niceSum = 0;
        $count = 0;

        if (count($this->reviews) > 0) {
            $this->hasReviews = true;
            foreach ($this->reviews as $review) {
                $paySum += $review->pay;
                $easeSum += $review->ease;
                $niceSum += $review->nice;
                $count++;
            }

            $this->reviewCount = $count;
            $this->payAvg = round($paySum / $count, 1);
            $this->easeAvg = round($easeSum / $count, 1);
            $this->niceAvg = round($niceSum / $count, 1);
            $this->overallAvg = round(
                .5 * $this->payAvg +
                .25 * $this->easeAvg +
                .25 * $this->niceAvg, 1);

            $this->payPercent = round(($paySum / $count) / self::ratingSteps * 100);
            $this->easePercent = round(($easeSum / $count) / self::ratingSteps * 100);
            $this->nicePercent = round(($niceSum / $count) / self::ratingSteps * 100);
        }
    }

}
