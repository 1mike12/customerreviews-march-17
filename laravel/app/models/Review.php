<?php
class Review extends Eloquent {

    protected $softDelete = true;
    protected $guarded = ["photo"];
    public static $rules = [
        "name" => "required",
        "weight" => "numeric"
    ];
    public function getCreatedDate($format="m/d/Y"){
        $date= new DateTime($this->created_at);
        return $date->format($format);
    }
    
    public function user(){
        return $this->belongsTo("User");
    }
}