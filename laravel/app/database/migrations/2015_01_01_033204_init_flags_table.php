<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitFlagsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create("flags", function($table) {
            $table->increments('id');
            $table->integer("user_id")->index()->unsigned();
            $table->integer("review_id")->index()->unsigned();

            $table->text("comment");

            $table->tinyInteger("category")->unsigned();
            $table->tinyInteger("status")->unsigned();

            //end stuff
            $table->timestamps();
            $table->softDeletes();

            //foreign keys
            $table->foreign('user_id')
                    ->references('id')->on('users')
                    ->onDelete("cascade")->onUpdate("cascade");
            $table->foreign('review_id')
                    ->references('id')->on('reviews')
                    ->onDelete("cascade")->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists("flags");
    }

}
