<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create("users", function($table) {
            $table->increments("id");
            $table->string("company_name", 50);
            $table->string("domain", 128);
            $table->string("address1", 100);
            $table->string("address2", 100)->nullable();
            $table->string("city", 50);
            $table->string("state", 2);
            $table->string("zip", 16);//
            $table->string("default_zip", 16)->nullable();//
            $table->string("phone", 16);//
           
            ////cannot be longer than 254
            //http://stackoverflow.com/a/1199238/2532762
            $table->string("email", 254);
            
            $table->string("password", 128);
            $table->string("remember_token", 100)->nullable();

            $table->string("confirmation", 128)->nullable();
            $table->boolean("confirmed");
            $table->boolean("vetted");//whether the business checks out
            
            //do at the end
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists("users");
    }

}
