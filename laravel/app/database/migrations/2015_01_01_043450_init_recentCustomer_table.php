<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitRecentCustomerTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create("recent_customers", function($table) {
            $table->increments('id');
            $table->integer("user_id")->index()->unsigned();
            $table->integer("customer_id")->index()->unsigned();
            
            $table->timestamps();
            $table->softDeletes();
            
            //foreign keys
            $table->foreign('user_id')
                    ->references('id')->on('users')
                    ->onDelete("cascade")->onUpdate("cascade");
            $table->foreign('customer_id')
                    ->references('id')->on('customers')
                    ->onDelete("cascade")->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists("recent_customers");
    }

}
