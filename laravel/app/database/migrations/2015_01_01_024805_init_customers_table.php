<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitCustomersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create("customers", function($table) {
            $table->increments('id');
            $table->string("last_name", 50);
            $table->string("first1", 50);
            $table->string("first2", 50)->nullable();
            
            $table->string("address1", 100);
            $table->string("address2", 100)->nullable();
            $table->string("city", 50);
            $table->string("state", 2);
            $table->string("zip", 16);
            
            
            //end stuff
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists("customers");
    }

}
