<?php

class CustomerSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $tableName = "customers";

        Eloquent::unguard();
        DB::table($tableName)->delete();

        $faker = Faker\Factory::create();
        $faker->seed(112);

        foreach ($this->family as $entry) {
            DB::table($tableName)->insert($entry);
        }

        for ($i = 0; $i < 500; $i++) {
            $entry = [
                "last_name" => $faker->lastName,
                "first1" => $faker->firstNameMale,
                "first2" => $faker->firstNameFemale,
                "address1" => $faker->streetName . " " . $faker->streetSuffix,
                "address2" => $faker->optional()->secondaryAddress,
                "city" => $faker->city,
                "state" => $faker->stateAbbr,
//                "zip" => $faker->randomElement(["01545", "03079", "02108"]),
                "zip" => $faker->postcode,
                "created_at" => $faker->dateTime($max = "2015-01-01 03:26:57")
            ];
            DB::table($tableName)->insert($entry);
        }
    }

    private $family = [
        //famous houses=============================
        [
            "last_name" => "Soprano",
            "first1" => "Tony",
            "first2" => "Carmela",
            "address1" => "14 Aspen Dr",
            "address2" => "",
            "city" => "North Caldwell",
            "state" => "NJ",
            "zip" => "07006",
            "created_at" => "2015-01-01 03:26:57"
        ]
        ,
        [
            "last_name" => "Obama",
            "first1" => "Barack",
            "first2" => "Michelle",
            "address1" => "1600 Pennsylvania Ave NW",
            "address2" => "",
            "city" => "Washington",
            "state" => "DC",
            "zip" => "20006",
            "created_at" => "2015-01-01 03:26:57"
        ]
        ,
        [
            "last_name" => "Smith",
            "first1" => "Will",
            "first2" => "Carlton",
            "address1" => "251 North Bristol Avenue",
            "address2" => "",
            "city" => "Los Angeles",
            "state" => "CA",
            "zip" => "90049",
            "created_at" => "2015-01-01 03:26:57"
        ]
        ,
        [
            "last_name" => "White",
            "first1" => "Walter",
            "first2" => "Skyler",
            "address1" => "3828 Piermont Dr",
            "address2" => "",
            "city" => "Albuquerque",
            "state" => "NM",
            "zip" => "87112",
            "created_at" => "2015-01-01 03:26:57"
        ]
        ,
        [
            "last_name" => "Schrader",
            "first1" => "Hank",
            "first2" => "marie",
            "address1" => "4915 cumbre del sur ct",
            "address2" => "",
            "city" => "Albuquerque",
            "state" => "NM",
            "zip" => "87111",
            "created_at" => "2015-01-01 03:26:57"
        ]
        ,
        [
            "last_name" => "Pinkman",
            "first1" => "Jesse",
            "first2" => "",
            "address1" => "322 16th St",
            "address2" => "",
            "city" => "Albuquerque",
            "state" => "NM",
            "zip" => "87104",
            "created_at" => "2015-01-01 03:26:57"
        ]
        ,
        [
            "last_name" => "Holmvik",
            "first1" => "Anders",
            "first2" => 'Adam "the laugh giver"',
            "address1" => "15020 hamlin st",
            "address2" => "",
            "city" => "Los Angeles",
            "state" => "CA",
            "zip" => "91411",
            "created_at" => "2015-01-01 03:26:57"
        ]
        ,
        [
            "last_name" => "Thiel",
            "first1" => "Peter",
            "first2" => '',
            "address1" => "13323 Wendover Terrace",
            "address2" => "",
            "city" => "San Diego",
            "state" => "CA",
            "zip" => "92130",
            "created_at" => "2015-01-01 03:26:57"
        ]
        ,
        [
            "last_name" => "Thiel",
            "first1" => "Peter",
            "first2" => '',
            "address1" => "13323 Wendover Terrace",
            "address2" => "",
            "city" => "San Diego",
            "state" => "CA",
            "zip" => "92130",
            "created_at" => "2015-01-01 03:26:57"
        ]
        ,
        //Shrewsbury==============================
        [
            "last_name" => "lee",
            "first1" => "kevin",
            "first2" => "sampson",
            "address1" => "59 O'donnel dr",
            "address2" => "",
            "city" => "shrewsbury",
            "state" => "ma",
            "zip" => "01545",
            "created_at" => "2015-01-01 03:26:57"
        ],
        [
            "last_name" => "brosnan",
            "first1" => "eileen",
            "first2" => "",
            "address1" => "8 dana rd",
            "address2" => "",
            "city" => "shrewsbury",
            "state" => "ma",
            "zip" => "01545",
            "created_at" => "2015-01-01 03:26:57"
        ],
        [
            "last_name" => "clark",
            "first1" => "andrew",
            "first2" => "lisa",
            "address1" => "17 candlewood way",
            "address2" => "",
            "city" => "shrewsbury",
            "state" => "ma",
            "zip" => "01545",
            "created_at" => "2015-01-01 03:26:57"
        ],
        [
            "last_name" => "brand",
            "first1" => "Jaqueline",
            "first2" => "Richard",
            "address1" => "5 minot ave",
            "address2" => "",
            "city" => "shrewsbury",
            "state" => "ma",
            "zip" => "01545",
            "created_at" => "2015-01-01 03:26:57"
        ]
        ,
        [
            "last_name" => "Qin",
            "first1" => "Robin",
            "first2" => "Lisa",
            "address1" => "3 minot ave",
            "address2" => "",
            "city" => "shrewsbury",
            "state" => "ma",
            "zip" => "01545",
            "created_at" => "2015-01-01 03:26:57"
        ]
        ,
        [
            "last_name" => "Vaillant",
            "first1" => "Annette",
            "first2" => "Richard",
            "address1" => "4 Heatherwood Dr",
            "address2" => "",
            "city" => "shrewsbury",
            "state" => "ma",
            "zip" => "01545",
            "created_at" => "2015-01-01 03:26:57"
        ]
        ,
        //Southborough Neighborhood===========================
        [
            "last_name" => "qin",
            "first1" => "robin",
            "first2" => "xia",
            "address1" => "35 darlene dr",
            "address2" => "",
            "city" => "southborough",
            "state" => "ma",
            "zip" => "01772",
            "created_at" => "2015-01-01 03:26:57"
        ],
        [
            "last_name" => "Fournier",
            "first1" => "richard",
            "first2" => "karen",
            "address1" => "33 darlene Dr",
            "address2" => "",
            "city" => "southborough",
            "state" => "ma",
            "zip" => "01772",
            "created_at" => "2015-01-01 03:26:57"
        ]
        ,
        [
            "last_name" => "Dahlstrom",
            "first1" => "Diane",
            "first2" => "Todd",
            "address1" => "37 darlene Dr",
            "address2" => "",
            "city" => "southborough",
            "state" => "ma",
            "zip" => "01772",
            "created_at" => "2015-01-01 03:26:57"
        ]
        ,
        [
            "last_name" => "Burke",
            "first1" => "Jon",
            "first2" => "Colleen",
            "address1" => "31 darlene Dr",
            "address2" => "",
            "city" => "southborough",
            "state" => "ma",
            "zip" => "01772",
            "created_at" => "2015-01-01 03:26:57"
        ]
        ,
        [
            "last_name" => "Fagan",
            "first1" => "Richard",
            "first2" => "Iryna",
            "address1" => "29 darlene Dr",
            "address2" => "",
            "city" => "southborough",
            "state" => "ma",
            "zip" => "01772",
            "created_at" => "2015-01-01 03:26:57"
        ]
        ,
        [
            "last_name" => "Johnson",
            "first1" => "Brett",
            "first2" => "Wendy",
            "address1" => "27 darlene Dr",
            "address2" => "",
            "city" => "southborough",
            "state" => "ma",
            "zip" => "01772",
            "created_at" => "2015-01-01 03:26:57"
        ]
        ,
        [
            "last_name" => "Donahue",
            "first1" => "Paul",
            "first2" => "Jayne",
            "address1" => "25 darlene Dr",
            "address2" => "",
            "city" => "southborough",
            "state" => "ma",
            "zip" => "01772",
            "created_at" => "2015-01-01 03:26:57"
        ]
        ,
        [
            "last_name" => "Pirozzi",
            "first1" => "James",
            "first2" => "Patricia",
            "address1" => "21 darlene Dr",
            "address2" => "",
            "city" => "southborough",
            "state" => "ma",
            "zip" => "01772",
            "created_at" => "2015-01-01 03:26:57"
        ]
        ,
        [
            "last_name" => "Goguen",
            "first1" => "Gary",
            "first2" => "Katherine",
            "address1" => "19 darlene Dr",
            "address2" => "",
            "city" => "southborough",
            "state" => "ma",
            "zip" => "01772",
            "created_at" => "2015-01-01 03:26:57"
        ]
        ,
        [
            "last_name" => "Perreault",
            "first1" => "Stephanie",
            "first2" => "Mark",
            "address1" => "17 darlene Dr",
            "address2" => "",
            "city" => "southborough",
            "state" => "ma",
            "zip" => "01772",
            "created_at" => "2015-01-01 03:26:57"
        ],
        [
            "last_name" => "Aroian",
            "first1" => "Richard",
            "first2" => "Lucy",
            "address1" => "14 darlene Dr",
            "address2" => "",
            "city" => "southborough",
            "state" => "ma",
            "zip" => "01772",
            "created_at" => "2015-01-01 03:26:57"
        ]
        ,
        [
            "last_name" => "Burstein",
            "first1" => "Jay",
            "first2" => "Maria",
            "address1" => "16 darlene Dr",
            "address2" => "",
            "city" => "southborough",
            "state" => "ma",
            "zip" => "01772",
            "created_at" => "2015-01-01 03:26:57"
        ]
        ,
    ];

}