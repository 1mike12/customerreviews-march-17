<?php

class ReviewSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $tableName="reviews";
        Eloquent::unguard();
        //delete all users
        DB::table($tableName)->delete();

        $faker = Faker\Factory::create();
        $faker->seed(112);
        $users = count(
                DB::table('users')
                        ->select(DB::raw('count(*) as user_count'))
                        ->get()
        );
        $customers = count(
                DB::table('customers')
                        ->select(DB::raw('count(*) as customer_count'))
                        ->get()
        );

        for ($i = 0; $i < 500; $i++) {
            $entry = [
                "user_id" => $faker->numberBetween(1, 50),
                "customer_id" => $faker->numberBetween(1, 100),
                "pay" => $faker->numberBetween(0, 5),
                "ease" => $faker->numberBetween(0, 5),
                "nice" => $faker->numberBetween(0, 5),
                "content" => $faker->paragraph($faker->numberBetween(1, 6)),
                "created_at" => $faker->dateTimeBetween($startDate = '-1 years', $endDate = '2015-01-01 03:26:57')
            ];
            DB::table($tableName)->insert($entry);
        }
    }

}
