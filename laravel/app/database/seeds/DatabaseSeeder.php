<?php

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Eloquent::unguard();

        $this->call('UserSeeder');
        $this->command->info('users table seeded!');
        
        $this->call('CustomerSeeder');
        $this->command->info('customers table seeded!');
                
        $this->call("ReviewSeeder");
        $this->command->info('reviews table seeded!');
        
        $this->call("MycustomerSeeder");
        $this->command->info('my_customers table seeded!');
        
        $this->call("RecentcustomerSeeder");
        $this->command->info('recent_customers table seeded!');
    }

}
