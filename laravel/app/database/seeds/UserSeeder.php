<?php

class UserSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public $pp = [
        "company_name" => "Pied Piper",
        "domain" => "piedpiper.com",
        "address1" => "1 main st",
        "address2" => "attn Pied Piper LLC",
        "city" => "San Jose",
        "state" => "CA",
        "zip" => "95111",
        "default_zip" => "01545",
        "phone" => "555-867-5309",
        "email" => "admin@piedpiper.com",
        "password" => "",
        "confirmation" => 1,
        "vetted" => 1,
        "created_at" => "2015-01-01 03:26:57"
    ];

    public function run() {
        Eloquent::unguard();
        //delete all users
        DB::table('users')->delete();

        $faker = Faker\Factory::create();
        $faker->seed(112);

        //setting up piped piper user
        $this->pp["password"] = Hash::make("12345");
        User::create($this->pp);

        for ($i = 0; $i < 100; $i++) {
            $companyName = $faker->company;
            $user = [
                "company_name" => $companyName,
                "domain" => $companyName . ".com",
                "address1" => $faker->streetAddress,
                "address2" => $faker->optional()->secondaryAddress,
                "city" => $faker->city,
                "state" => $faker->stateAbbr,
                "zip" => $faker->postcode,
                "default_zip" => $faker->optional()->postcode,
                "phone" => $faker->unique()->phoneNumber,
                "email" => $faker->unique()->companyEmail,
                "password" => "",
                "confirmation" => 1,
                "vetted" => 1,
                "created_at" => $faker->dateTime($max = "2015-01-01 03:26:57")
            ];
            User::create($user);
        }
    }

}
