<?php

class MycustomerSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $tableName = "my_customers";

        Eloquent::unguard();
        //delete all users
        DB::table($tableName)->delete();

        $faker = Faker\Factory::create();
        $faker->seed(112);

        for ($i = 0; $i < 10; $i++) {
            $entry = [
                "user_id" => 1,
                "customer_id" => $i + 1,
                "note" => $faker->paragraph(1),
                "created_at" => $faker->dateTimeBetween($startDate = '2014-01-01 03:26:57', $endDate = '2015-01-01 03:26:57')
            ];
            DB::table($tableName)->insert($entry);
        }
    }

}
