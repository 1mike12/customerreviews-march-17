<?php

class RecentcustomerSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $tableName="recent_customers";
        
        Eloquent::unguard();
        //delete all users
        DB::table($tableName)->delete();

        $faker = Faker\Factory::create();
        $faker->seed(112);

        for ($i = 0; $i < 10; $i++) {
            $entry = [
                "user_id" => 1,
                "customer_id" => $faker->numberBetween(1, 100),
                "created_at" => $faker->dateTimeBetween($startDate = '2014-01-01 03:26:57', $endDate = '2015-01-01 03:26:57')
            ];
            DB::table($tableName)->insert($entry);
        }
    }

}
