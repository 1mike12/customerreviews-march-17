<?php

class CustomerController extends BaseController {

    protected $layout = "layouts.default";

    public function getAddcustomer() {
        View::inject("content", View::make("customer.addCustomer.addCustomer"));
    }

    //has to not be index because no parameter passing into getIndex
    public function getCustomer($id) {
        $data = [];
        if (isset($id)) {
            $customer = Customer::where("id", "=", $id)->with(["reviews" => function($query) {
                            $query->orderBy("created_at", "DESC");
                        }])->first();
            $customer->loadAverages();
            RecentCustomersController::addToRecent();
            //if customer exists
            if (count($customer)) {
                $data["customer"] = $customer;
                View::inject("content", View::make("customer.customer")->with($data));
            }

            //customer doesnt exist
            else {
                View::inject("content", View::make("customer.customerDNE")->with($data));
            }
        }
        //TODO: 1mike12  no id set 
        else {
            
        }
    }

}
