<?php

class SearchController extends BaseController {

    protected $layout = "layouts.default";

    public function getSearch() {
        $name = Input::get("name");
        $location = Input::get("location");

        //both name and location=======================================
        if ($name != "" && $location != "") {
            $data = ["message" => ""];
            //Location is zip code exact, valid name given
            if (SearchController::isZip($location) && SearchController::validName($name)) {
                $zip = substr(SearchController::isZip($location), 0, 5);
                $customers = Customer::
                                where("last_name", "=", $name)
                                ->where("zip", "=", $zip)->take(20)->get();//1mike12 change to fuzzy search
                $data["customers"] = $customers;
            }

            //location is exact state
        }


        //only name filled in - use Default ZIP/IP======================
        elseif ($name != "" && $location == "") {
            $data = [];
            //user logged in
            if (Auth::check()) {
                $zip = Auth::user()->default_zip;
                $customers = Customer::
                                where("last_name", "=", $name)
                                ->where("zip", "=", $zip)->take(20)->get();//1mike12 change to fuzzy search
                $data["customers"] = $customers;
                $data["message"] = "using your default zip code $zip";
            }
        }


        //only location filled in========================================
        elseif ($name == "" && $location != "") {
            $data = [];
            //is a zip format============================
            if ($this->isZip($location)) {
                $zip = $this->isZip($location);

                $customers = Customer::where("zip", "=", $zip)->with("reviews")->take(20)->take(20)->get();
                $data["customers"] = $customers;
            }
            //is a town, state format====================
            elseif ($this->splitTownState($location)) {
                $result = $this->splitTownState($location);

                $city = $result["city"];
                $state = $result["state"];

                //real state
                if ($this->isState($state)) {
                    //convert state to MA
                    $state = $this->isState($state);

                    $town_state = Town::
                                    where("city", "=", $city)->
                                    where("admin_1_short", "=", $state)->get();
                    //town exists with matching state
                    if (!$town_state->isEmpty()) {
                        $customers = Customer::
                                        where("city", "=", $city)->
                                        where("state", "=", $state)->take(20)->get();
                        $data["customers"] = $customers;
                    } else {
                        $matchedTowns = Town::where("city", "=", $city)->get();
                        //town exists, wrong state
                        if (!$matchedTowns->isEmpty()) {
                            $data["message"] = "$city not found in $state. Did you mean the following towns?";
                            $data["towns"] = $matchedTowns;
                        }
                        //city doesn't exist
                        else {
                            $data["message"] = "We couldn't find $city."; //1mike12 change to include suggested towns??
                        }
                    }
                }

                //invalid state 
                else {
                    $matchedTowns = Town::where("city", "=", $city)->get();
                    //town exists
                    if (!$matchedTowns->isEmpty()) {
                        $data["message"] = "$city not found in $state. Did you mean the following towns?";
                        $data["towns"] = $matchedTowns;
                    }
                }
            }
            //invalid location=========================
            else {
                
            }
        }


        //nothing filled in (handle w/ frontend javascript)
        else {
            
        }


        View::inject("content", View::make("search.results")->with($data));
    }

    /**
     * truthy for "12345" "12345-1234" "12345 " 
     * 
     * returns 12345 or false
     */
    private function isZip($input) {
        //remove trailing spaces
        $cleaned = str_replace(" ", "", $input);
        $zip = preg_match("/^[0-9]{5}$/", $cleaned);
        $zipExpanded = preg_match("/^[0-9]{5}-[0-9]{4}$/", $cleaned);

        //only return 5 digit zip code
        if ($zip || $zipExpanded) {
            return $cleaned;
        } else {
            return false;
        }
    }

    /**
     * input "ma" "MA" "MassachUseTTS"
     * returns "MA"
     */
    private function isState($input) {
        $input = strtoupper($input);
        //check == MASSACHUSETTS
        if (array_key_exists($input, Constants::$statesUpper)) {
            return $input;
        }
        //check == MA
        elseif (in_array($input, Constants::$statesUpper)) {
            //returns key if value exists
            return array_search($input, Constants::$statesUpper);
        }
        return false;
    }

    /**
     * input shrewsbury ma
     * returns [shrewsbury, ma]
     */
    private function splitTownState($input) {
        $splitResults = preg_split("/[\s,]+/", $input);

        //make sure there are only 2 parts
        if (sizeof($splitResults) == 2) {
            $city = $splitResults[0];
            $state = $this->isState($splitResults[1]);
            return ["city" => $city, "state" => $state];
        } else {
            return false;
        }
    }

    private function validName($name) {
        $hasNumber = preg_match("/\d/", $name);

        if ($hasNumber) {
            return false;
        } else {
            return true;
        }
    }

    public function postTownHint() {
        $query = Input::get("query");
        $towns = Town::where("city", "LIKE", "$query%")->groupBy("town", "admin_1")->take(5)->get();
        $hints = [];
        foreach ($towns as $town) {
            $hints[] = "$town->town, $town->admin_1";
        }
        return $towns;
    }

}
