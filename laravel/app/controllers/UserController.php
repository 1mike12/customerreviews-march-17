<?php

class UserController extends BaseController {

    public function postSignin() {
        if (Auth::attempt(['email' => Input::get('email'), 'password' => Input::get('password')])) {
            //redirect to original blocked path or default to dashboard
            return Redirect::intended("dash");
        } else {
            return Redirect::back()
                            ->with('message', 'Your username/password combination was incorrect')
                            ->withInput();
        }
    }

    public function getRegister() {
        return View::make("user.register");
    }

    public function postRegister() {
        //edit email to contain domain
        $input = Input::all();
        $input["email"] = $input["email"] . "@" . $input["domain"];

        $validator = Validator::make($input, User::$rules, User::$messages);
        if ($validator->passes()) {
            $user = new User($input);
            $user->default_zip = $user->zip;
            $user->password = Hash::make(Input::get("password"));

            //check that there are no confirmation code collisions
            do {
                $confirmation = str_random(128);
                $collection = User::where("confirmation", "=", $confirmation)->get();
            } while (!$collection->isEmpty());

            $user->confirmation = $confirmation;
            $user->confirmed = 0;
            $user->save();

            //send verification email
            UserController::sendConfirmation($user);

            return Redirect::to("user/register-success")->with("email", $user->email);
        } else {
            return Redirect::to("user/register")
                            ->withErrors($validator)->withInput()
                            ->with("submitted", true);
        }
    }

    public static function sendConfirmation($user) {
        Mail::send("user.confirm-email", ["user" => $user], function($message) use ($user) {
            $message->from("noreply@" . DOMAIN, COMPANY);
            $message->to($user->email, $user->company_name)
                    ->subject(COMPANY . 'Verification email');
        });
    }

    public function getConfirm($confirmation = "default") {
        $user = User::where("confirmation", "=", $confirmation)->first();
        if ($user) {
            $user->confirmed = 1;
            $user->save();
            View::inject("content", View::make("user.confirmSuccess"));
        } else {
            View::inject("content", View::make("user.confirmFail"));
        }
    }

    //1mike12 needs work
    //validate email being given
    public function postResend() {
        $email = Input::get("email");
        if ($email) {
            $user = User::where("email", "=", $email)->first();
            if ($user) {
                UserController::sendConfirmation($user);
            }
        } else {
            //return no email supplied
        }
    }

    public function getRegisterSuccess() {
        View::inject("content", View::make("user.registerSuccess"));
    }

    public function getSignout() {
        Auth::logout();
        return Redirect::to("/")->with("success", "You are signed out!");
    }

    public function getEdit() {
        View::inject("content", View::make("user.edit"));
    }

}
