<?php

class DatabaseController extends BaseController {

    protected $tables = ["users", "reviews", "customers"];

    public function getDown() {
        DB::statement("SET FOREIGN_KEY_CHECKS=0;");

        foreach ($this->tables as $table) {
            Schema::dropIfExists($table);
        }
        return "tables dropped";
    }

    public function getUp($table) {
        if ($table == "users") {
            DatabaseController::users();
        } elseif ($table == "customers") {
            DatabaseController::customers();
        } elseif ($table == "reviews") {
            DatabaseController::reviews();
        }elseif($table =="towns"){
            DatabaseController::towns();
        } else {
            DatabaseController::users();
            DatabaseController::customers();
            DatabaseController::reviews();
        }
    }

    public static function towns() {
        Schema::create("towns", function($table) {
            //order to match free database http://download.geonames.org/export/dump/
            $table->increments("id");
            $table->string("country", 5);//US
            $table->string("postal_code", 5);
            $table->string("town", 128);
            $table->string("admin_1", 128);//CA
            $table->string("admin_1_short", 8);//CA
            $table->string("admin_2", 128);//santa clara
            $table->integer("admin_2_id");
            $table->decimal("lat", 7, 4);
            $table->decimal("lng", 7, 4);
        });
    }

    public static function users() {
        Schema::create("users", function($table) {
            $table->increments("id");
            $table->string("name", 128);
            $table->string("domain", 255);
            $table->string("address1", 255);
            $table->string("address2", 255)->nullable();
            $table->string("city", 128);
            $table->string("state", 64);
            $table->string("zip", 16);
            $table->string("default_zip", 12)->nullable();
            $table->string("phone", 128);

            $table->string("email", 255);
            $table->string("password", 255);
            $table->string("remember_token", 100)->nullable();

            $table->string("confirmation", 128)->nullable();
            $table->tinyInteger("confirmed");

            $table->timestamps();
            $table->softDeletes();
        });
    }

    public static function customers() {
        Schema::create("customers", function($table) {
            $table->increments('id');
            $table->string("last_name", 128);

            $table->string("address1", 255);
            $table->string("address2", 255)->nullable();
            $table->string("city", 128);
            $table->string("state", 64);
            $table->string("zip", 16);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    public static function reviews() {
        Schema::create("reviews", function($table) {
            $table->increments('id');
            $table->integer("user_id")->index()->unsigned();
            $table->integer("household_id")->index()->unsigned();

            $table->tinyInteger("rating");
            $table->text("content");

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')
                    ->references('id')->on('users')
                    ->onDelete("cascade")->onUpdate("cascade");
            $table->foreign('customer_id')
                    ->references('id')->on('customers')
                    ->onDelete("cascade")->onUpdate("cascade");
        });
    }

}
