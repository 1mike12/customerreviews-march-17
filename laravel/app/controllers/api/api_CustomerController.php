<?php

class api_CustomerController extends \BaseController {

    //set resource name, used in error messages
    public $resourceName = "best_customer";

    /**
     * Return array of customer objects
     *
     * @return Response
     */
    public function index() {
        $customer = Customer::with("reviews")->get();

        $payload = [
            "status" => null,
            "message" => null,
            "customer" => null
        ];

        if (count($customer)) {
            $payload["status"] = "success";
            $payload["customer"] = $customer;
        } else {
            $payload["status"] = "empty";
            $payload["message"] = "no customer added to recent_customer";
        }


        return $payload;
    }

    public function show($customer_id) {
        $customer = Customer::where("id", "=", $customer_id)
                        ->with("reviews.user")
                        ->get()[0];

        $myCustomer = MyCustomer::where("user_id", "=", Auth::id())
                ->where("customer_id", "=", $customer_id)
                ->get();



        $payload = [
            "status" => null,
            "isMyCustomer" => null,
            "message" => null,
            "customer" => null,
        ];

        if (count($customer)) {
            $isMyCustomer = count($myCustomer) ? true : false;
            $customer->isMyCustomer = $isMyCustomer;
            $payload["status"] = "success";
            $payload["customer"] = $customer;
        } else {
            $payload["status"] = "empty";
            $payload["message"] = "customer with id :$customer_id not found";
        }


        return $payload;
    }

}
