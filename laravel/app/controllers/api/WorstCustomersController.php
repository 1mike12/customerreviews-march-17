<?php

class WorstCustomersController extends \BaseController {

    public $resourceName = "worst_customers";

    /**
     * Return array of customer objects
     *
     * @return Response
     */
    public function index() {
        $defaultZip = Auth::user()->default_zip;
        $unsorted = Customer::where("zip", "=", $defaultZip)
                ->has("reviews")
                ->with("reviews")
                ->get();
        foreach($unsorted as $u){
            $u->loadAverages();
        }
        
        $customers = $unsorted->sortBy("overallAvg")
                ->splice(0, 5);
        
        $payload = [
            "status" => null,
            "message" => null,
            "customers" => null
        ];

        if (count($customers)) {
            $payload["status"] = "success";
            $payload["customers"] = $customers;
        } else {
            $payload["status"] = "empty";
            $payload["message"] = "no customers added to recent_customers";
        }


        return $payload;
    }

}
