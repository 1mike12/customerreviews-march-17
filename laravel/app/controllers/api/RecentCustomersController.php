<?php

class RecentCustomersController extends \BaseController {

    public $resourceName = "recent_customers";

    /**
     * Return array of customer objects
     *
     * @return Response
     */
    public function index() {
        $customers = Auth::user()->recentCustomers;
        $payload = [
            "status" => null,
            "message" => null,
            "customers" => null
        ];

        if (count($customers)) {
            $payload["status"] = "success";
            $payload["customers"] = $customers;
        } else {
            $payload["status"] = "empty";
            $payload["message"] = "no customers added to $this->resourceName";
        }


        return $payload;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        $payload = [
            "status" => null,
            "message" => null,
        ];

        //premium feature for the future - disable adding to my_customers if > ie 50
        $validator = Validator::make(Input::all(), MyCustomer::rules);
        $id = Input::get("customer_id");

        if (!count(Customer::find($id))) {
            $payload["status"] = "error";
            $payload["message"] = "trying to add a non existing customer to $this->resourceName. Customer.id: $id DNE";
        } else if ($validator->passes()) {
            Eloquent::unguard();
            $my_customer = new MyCustomer(Input::all());
            $my_customer->user_id = Auth::id();
            Eloquent::reguard();

            $my_customer->save();
        }
    }
    
    public static function addToRecent (){
        
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id) {
        $payload = [
            "status" => null,
            "message" => null,
        ];
        $validator = Validator::make(Input::all(), MyCustomer::$rules);
        $my_customer = MyCustomer::find($id);

        //my_customer exists? Count() 
        //returns 0 for empty collection, null for invalid model
        if (!count($my_customer)) {
            $payload["status"] = "error";
            $payload["message"] = "$this->resourceName with id:$id does not exist";
        } //is the user authorized to change this my_customer?
        else if ($my_customer->user_id !== Auth::id()) {
            $payload["status"] = "error";
            $payload["message"] = "unauthorized to update $this->resourceName with id: $id";
        } //matches rules pf MyCustomer Model
        else if ($validator->fails()) {
            $payload["status"] = "error";
            //returns {"field_name":[], "field2":[]}
            $payload["mesage"] = $validator->errors()->getMessages();
        } else {

            $payload["status"] = "success";
            $input = Input::get("note");
            $my_customer->note = Input::get("note");
            $my_customer->save();
        }
        return $payload;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id) {
        $payload = [
            "status" => null,
            "message" => null,
        ];
        $my_customer = MyCustomer::find($id);

        //is the user authorized to change this my_customer?
        if ($my_customer->user_id !== Auth::id()) {
            $payload["status"] = "error";
            $payload["message"] = "unauthorized to delete $this->resourceName with id: $id";
        } else {

            $payload["status"] = "success";
            $my_customer->delete();
        }
        return $payload;
    }

}
