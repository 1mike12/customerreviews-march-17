<?php

class DashController extends BaseController {

    protected $layout = "layouts.default";

    public function getIndex() {

        View::inject("content", View::make("dash.dash"));
    }

}
