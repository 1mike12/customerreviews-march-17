

<div class="terms-wrapper">
         <h2>Privacy Policy</h2>
           <div class="space-sm"></div> 
        <div class="panel panel-default">
            <div class="panel-body">
                <!-- Replace this-->
                <!-- START PRIVACY POLICY CODE -->
                <div style="font-family:arial"><strong>What information do we collect?</strong>
                    <br /><br />We collect information from you when you register on our site, fill out a form or customer reviews.
                    <br /><br />When ordering or registering on our site, as appropriate, you may be asked to enter your: name, e-mail
                    address, mailing address, phone number or company domain name. You may, however, visit our site anonymously.
                    <br /><br /><strong>What do we use your information for?</strong> <br /><br />
                    Any of the information we collect from you may be used in one of the following ways: <br /><br />
                    ; To personalize your experience<br />(your information helps us to better respond to your individual needs)
                    <br /><br />; To improve customer service<br />(your information helps us to more effectively respond to your
                    customer service requests and support needs)<br /><br /><strong>How do we protect your information?</strong>
                    <br /><br />We implement a variety of security measures to maintain the safety of your personal information 
                    when you enter, submit, or access your personal information. <br /> <br />We offer the use of a secure serve
                    r. All supplied sensitive/credit information is transmitted via Secure Socket Layer (SSL) technology and then
                    encrypted into our Payment gateway providers database only to be accessible by those authorized with special 
                    access rights to such systems, and are required to?keep the information confidential.<br /><br />After a tran
                    saction, your private information (credit cards, social security numbers, financials, etc.) will not be stored
                    on our servers.<br /><br /><strong>Do we use cookies?</strong> <br /><br />Yes (Cookies are small files that a
                    site or its service provider transfers to your computers hard drive through your Web browser (if you allow) tha
                    t enables the sites or service providers systems to recognize your browser and capture and remember certain inf
                    ormation<br /><br /> We use cookies to understand and save your preferences for future visits.<br /><br /><strong>
                        Do we disclose any information to outside parties?</strong> <br /><br />We do not sell, trade, or otherwise
                    transfer to outside parties your personally identifiable information. This does not include trusted third
                    parties who assist us in operating our website, conducting our business, or servicing you, so long as thos
                    e parties agree to keep this information confidential. We may also release your information when we believ
                    e release is appropriate to comply with the law, enforce our site policies, or protect ours or others right
                    s, property, or safety. However, non-personally identifiable visitor information may be provided to other p
                    arties for marketing, advertising, or other uses.<br /><br /><strong>Third party links</strong> <br /><br /> 
                    Occasionally, at our discretion, we may include or offer third party products or services on our website. T
                    hese third party sites have separate and independent privacy policies. We therefore have no responsibility o
                    r liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integ
                    rity of our site and welcome any feedback about these sites.<br /><br /><strong>California Online Privacy P
                        rotection Act Compliance</strong><br /><br />Because we value your privacy we have taken the necessary p
                    recautions to be in compliance with the California Online Privacy Protection Act. We therefore will not 
                    distribute your personal information to outside parties without your consent.<br /><br /><strong>Childr
                        ens Online Privacy Protection Act Compliance</strong> <br /><br />We are in compliance with the req
                    uirements of COPPA (Childrens Online Privacy Protection Act), we do not collect any information fr
                    om anyone under 13 years of age. Our website, products and services are all directed to people wh
                    o are at least 13 years old or older.<br /><br /><strong>Terms and Conditions</strong> <br /><br />P
                    lease also visit our Terms and Conditions section establishing the use, disclaimers, and limitati
                    ons of liability governing the use of our website at <a href="URL">URL</a><br /><br /><strong>Your
                        Consent</strong> <br /><br />By using our site, you consent to our <a style='text-decoration:

                                                                                              none; color:#3C3C3C;' href='http://www.freeprivacypolicy.com/' target='_blank'>websites privacy po
                        licy</a>.<br /><br /><strong>Changes to our Privacy Policy</strong> <br /><br />If we d
                    ecide to change our privacy policy, we will post those changes on this page, and/or sen
                    d an email notifying you of any changes. <br /><br />This policy was last modified on 8/
                    7/2014<br /><br />
                </div>
            </div>
        </div>
    </div>

