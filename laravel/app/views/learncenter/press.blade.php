<div class="press-wrapper">
    <h2>Press</h2>

    <div class="space-sm"></div>

    <div class="panel panel-default">
        <div class="panel-body">
            <h3>Logo</h3>

            <div class="space-sm"></div>

            <div class="row">
                <div class="col-sm-6">
                    <center>
                        <img class="register-logo" src="{{URL::asset("images/logo-register.png")}}" alt="Error">
                    </center>
                </div>
                <div class="col-sm-6">
                    <center>
                        <img class="register-logo" src="{{URL::asset("images/logoheader.png")}}" alt="Error">
                    </center>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="bg-color1">
                        <center>
                            <img class="register-logo" src="{{URL::asset("images/logo-header-before.png")}}"
                                 alt="Error">
                        </center>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="bg-color2">
                        <center>
                            <img class="register-logo" src="{{URL::asset("images/logo-header-before.png")}}"
                                 alt="Error">
                        </center>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-body">
            <h3>Media Department</h3>

            <div class="space-sm"></div>
            <p><span class="glyphicon glyphicon-earphone"></span>&nbsp;&nbsp;603.401.1987</p>

            <p><span class="glyphicon glyphicon-envelope"></span>&nbsp;&nbsp;media@email.com</p>

            <p><span class="glyphicon glyphicon-map-marker"></span>&nbsp;&nbsp;UPS PO Box in Boston</p>
        </div>
    </div>
</div>