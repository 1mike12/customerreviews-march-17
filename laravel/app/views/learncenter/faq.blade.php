
<div class="faq-band">
    <h2>Find an Answer</h2>
      <div class="space-sm"></div> 
    <div class="panel-group" id="accordion">

        <!-- Title -->
        <!-- ======================================================== -->
        <div class="row">
            <div class="col-sm-12"><label>Privacy</label></div>
        </div>
        <!-- ====== -->            
 
        <div class="panel panel-default">
            <a data-toggle="collapse" data-parent="#accordion" href="#faq-whocanview">
                <div class="panel-heading">
                    <h4 class="panel-title"> 
                        Who can view my account?
                    </h4>
                </div>
            </a>
            <div id="faq-whocanview" class="panel-collapse collapse">
                <div class="panel-body">
                    Your account information is completely anonymous. Other users
                    (businesses) can only see your reviews, they will not know who
                    it was provided by. <a href=”#”>Learn more</a>
                </div>
            </div>                    
        </div>


        <div class="panel panel-default">
            <a data-toggle="collapse" data-parent="#accordion" href="#faq-private">
                <div class="panel-heading">
                    <h4 class="panel-title"> 
                        Is my information private?
                    </h4>
                </div>
            </a>
            <div id="faq-private" class="panel-collapse collapse">
                <div class="panel-body">
                    Yes, all business information provided is securely stored.
                </div>
            </div>                    
        </div>

        <div class="panel panel-default">
            <a data-toggle="collapse" data-parent="#accordion" href="#faq-hidden">
                <div class="panel-heading">
                    <h4 class="panel-title"> 
                        Can my clients see if I have reviewed them?  
                    </h4>
                </div>
            </a>
            <div id="faq-hidden" class="panel-collapse collapse">
                <div class="panel-body">
                    Your client would need an account that has passed our screening process.
                    If your client does have an account they will be able to see a review
                    given to them though they will not know who provided it.
                    <br/>
                    To ensure anonymity read over our <a href=”#”>review recommendations</a>

                </div>
            </div>                    
        </div>

        <div class="panel panel-default">
            <a data-toggle="collapse" data-parent="#accordion" href="#faq-postanonymously">
                <div class="panel-heading">
                    <h4 class="panel-title"> 
                        Can I post anonymously?
                    </h4>
                </div>
            </a>
            <div id="faq-postanonymously" class="panel-collapse collapse">
                <div class="panel-body">
                    Reviews are anonymous, though it is important not go give away specific
                    detail in the comment section such as the exact date you worked on the
                    project with the client.
                    <br/>
                    We recommend our users read our
                    <a href=”#”>”Review Recommendations”</a> to assure they remain anonymous.


                </div>
            </div>                    
        </div>

        <!-- Title -->
        <!-- ======================================================== -->
        <div class="space-md"></div>
        <div class="row">
            <div class="col-sm-12"><label>Rating</label></div>
        </div>
        <!-- ====== -->


        <div class="panel panel-default">
            <a data-toggle="collapse" data-parent="#accordion" href="#faq-rating">
                <div class="panel-heading">
                    <h4 class="panel-title"> 
                        How do the ratings work?
                    </h4>
                </div>
            </a>
            <div id="faq-rating" class="panel-collapse collapse">
                <div class="panel-body">
                    All reviews are averaged, though you are able to review individual
                    experiences for more detailed information. 
                </div>
            </div>                    
        </div>


        <div class="panel panel-default">
            <a data-toggle="collapse" data-parent="#accordion" href="#faq-delete">
                <div class="panel-heading">
                    <h4 class="panel-title"> 
                        How do I delete a review?
                    </h4>
                </div>
            </a>
            <div id="faq-delete" class="panel-collapse collapse">
                <div class="panel-body">
                    You must go to the “My Reviews” tab on your dash board where
                    you can edit or delete past reviews.
                </div>
            </div>                    
        </div>

        <!-- Title -->
        <!-- ======================================================== -->
         <div class="space-md"></div>
        <div class="row">
            <div class="col-sm-12"><label>Account</label></div>
        </div>
        <!-- ====== -->


        <div class="panel panel-default">
            <a data-toggle="collapse" data-parent="#accordion" href="#faq-needaccount">
                <div class="panel-heading">
                    <h4 class="panel-title"> 
                        Do I need to make an account to post?
                    </h4>
                </div>
            </a>
            <div id="faq-needaccount" class="panel-collapse collapse">
                <div class="panel-body">
                    Yes, to ensure accurate reviews all accounts must be registered as businesses.
                </div>
            </div>                    
        </div>

        <div class="panel panel-default">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse11">
                <div class="panel-heading">
                    <h4 class="panel-title"> 
                        Why is my account not yet verified?
                    </h4>
                </div>
            </a>
            <div id="collapse11" class="panel-collapse collapse">
                <div class="panel-body">

                    This could be due to several reasons. Most likely it is because you either:
                    <br/>
                    -	Our system verification did not have enough information to determine you companies legitimacy
                    <br/>
                    -	Or our system has temporarily flagged you for suspicious use until our support can further look into it

                    If you are a non-verified account you have a limited time (60days or under) to
                    provide reasonable information before your account is terminated. If you are
                    experiencing any difficulty with this please contact our “support team”.

                </div>
            </div>                    
        </div>

        <!-- Title -->
        <!-- ======================================================== -->
        <div class="space-md"></div>
        <div class="row">
            <div class="col-sm-12"><label>Legal</label></div>
        </div>
        <!-- ====== -->

        <div class="panel panel-default">
            <a data-toggle="collapse" data-parent="#accordion" href="#faq-libel">
                <div class="panel-heading">
                    <h4 class="panel-title"> 
                        Can I be held for libel? (slander)
                    </h4>
                </div>
            </a>
            <div id="faq-libel" class="panel-collapse collapse">
                <div class="panel-body">
                    No. As long as you abide by our <a href="#">“Terms,”</a> you cannot be held for libel.
                    <br/>
                    Reviews must be:
                    <br/>
                    -	Truthful and are of the best knowledge of the reviewer
                    <br/>
                    -	And are considered to be a personal opinion

                </div>
            </div>                    
        </div>

        <div class="panel panel-default">
            <a data-toggle="collapse" data-parent="#accordion" href="#faq-reviewrestrictions">
                <div class="panel-heading">
                    <h4 class="panel-title"> 
                        Are there privacy restrictions to how much info I can post about cliental?
                    </h4>
                </div>
            </a>
            <div id="faq-reviewrestrictions" class="panel-collapse collapse">
                <div class="panel-body">
                    Yes, stay away from providing any unnecessary information about the client experience.
                    <br/>
                    Visit our <a href=”#”>“Terms”</a>page for more information.

                </div>
            </div>                    
        </div>

        <!-- Title -->
        <!-- ======================================================== -->
        <div class="space-md"></div>
        <div class="row">
            <div class="col-sm-12"><label>General</label></div>
        </div>
        <!-- ====== -->

        <div class="panel panel-default">
            <a data-toggle="collapse" data-parent="#accordion" href="#faq-goal">
                <div class="panel-heading">
                    <h4 class="panel-title"> 
                        What is the goal of this website?
                    </h4>
                </div>
            </a>
            <div id="faq-goal" class="panel-collapse collapse">
                <div class="panel-body">
                    We want to help businesses avoid harmful clients and find
                    particularly great ones to do business with.
                </div>
            </div>                    
        </div>

        <div class="panel panel-default">
            <a data-toggle="collapse" data-parent="#accordion" href="#faq-free">
                <div class="panel-heading">
                    <h4 class="panel-title"> 
                        Is {{COMPANY}} really free?
                    </h4>
                </div>
            </a>
            <div id="faq-free" class="panel-collapse collapse">
                <div class="panel-body">
                    Yes.
                </div>
            </div>                    
        </div>

        <div class="panel panel-default">
            <a data-toggle="collapse" data-parent="#accordion" href="#faq-ouner">
                <div class="panel-heading">
                    <h4 class="panel-title"> 
                        Who owns {{COMPANY}}'s website? 
                    </h4>
                </div>
            </a>
            <div id="faq-ouner" class="panel-collapse collapse">
                <div class="panel-body">
                    Clientype was developed and is owned by Mike Qin and Brian Gioia.
                    For information, visit our <a href=”#”>“Founders”</a>page.
                </div>
            </div>                    
        </div>

        <div class="panel panel-default">
            <a data-toggle="collapse" data-parent="#accordion" href="#faq-who">
                <div class="panel-heading">
                    <h4 class="panel-title"> 
                        Can anyone post?
                    </h4>
                </div>
            </a>
            <div id="faq-who" class="panel-collapse collapse">
                <div class="panel-body">
                    You must be a business to create an account and provide reviews. As Clientype is further 
                    developed we will be more accurate in our business verification processes.
                </div>
            </div>                    
        </div>

        <div class="panel panel-default">
            <a data-toggle="collapse" data-parent="#accordion" href="#faq-intended">
                <div class="panel-heading">
                    <h4 class="panel-title"> 
                        Is it strictly intended for businesses?
                    </h4>
                </div>
            </a>
            <div id="faq-intended" class="panel-collapse collapse">
                <div class="panel-body">
                    Yes, Clientype is intended for business use only.
                </div>
            </div>                    
        </div>

        <div class="panel panel-default">
            <a data-toggle="collapse" data-parent="#accordion" href="#faq-viewusers">
                <div class="panel-heading">
                    <h4 class="panel-title"> 
                        How do I navigate to other users account? Can I?
                    </h4>
                </div>
            </a>
            <div id="faq-viewusers" class="panel-collapse collapse">
                <div class="panel-body">
                    You are not capable of viewing other user accounts.
                </div>
            </div>                    
        </div>

        <div class="panel panel-default">
            <a data-toggle="collapse" data-parent="#accordion" href="#faq-customeraccount">
                <div class="panel-heading">
                    <h4 class="panel-title"> 
                        Can customers make an account?
                    </h4>
                </div>
            </a>
            <div id="faq-customeraccount" class="panel-collapse collapse">
                <div class="panel-body">
                    A customer would be required to be a business owner to access our database.
                </div>
            </div>                    
        </div>

        <div id="bing" class="space"></div>
        <div class="space"></div>

    </div>
</div>

