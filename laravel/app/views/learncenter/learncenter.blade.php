@extends("layouts.default")

@section("content")
    <div class="space-md"></div>

    <div class="learncenter-wrapper">
        <div class="container-fluid">

            <div role="tabpanel">
                <div class="col-sm-4">
                    <div class="sub-pennant-wrapper normal">
                        <ul class="nav nav-stacked" role="tablist">
                            <li role="presentation"><a href="#faq" aria-controls="faq" role="tab" data-toggle="tab">Find
                                    an Answer<i class="pull-right fa fa-angle-right"></i></a></li>
                            <li role="presentation"><a href="#testimonials" aria-controls="testimonials" role="tab"
                                                       data-toggle="tab">Testimonials<i
                                            class="pull-right fa fa-angle-right"></i></a></li>
                            <li role="presentation"><a href="#founders" aria-controls="founders" role="tab"
                                                       data-toggle="tab">Founders<i
                                            class="pull-right fa fa-angle-right"></i></a></li>
                            <li role="presentation"><a href="#press" aria-controls="press" role="tab" data-toggle="tab">Press<i
                                            class="pull-right fa fa-angle-right"></i></a></li>
                            <li role="presentation"><a href="#privacy" aria-controls="privacy" role="tab"
                                                       data-toggle="tab">Privacy Policy<i
                                            class="pull-right fa fa-angle-right"></i></a></li>
                            <li role="presentation"><a href="#terms" aria-controls="terms" role="tab" data-toggle="tab">Terms<i
                                            class="pull-right fa fa-angle-right"></i></a></li>
                            <li role="presentation"><a href="#contact" aria-controls="contact" role="tab"
                                                       data-toggle="tab">Contact Us<i
                                            class="pull-right fa fa-angle-right"></i></a></li>
                        </ul>
                    </div>
                    <div class="space-md"></div>
                </div>

                <div class="col-sm-8">
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="faq">@include("learncenter.faq")</div>
                        <div role="tabpanel" class="tab-pane" id="testimonials">@include("learncenter.testimonials")</div>
                        <div role="tabpanel" class="tab-pane" id="founders">@include("learncenter.founders")</div>
                        <div role="tabpanel" class="tab-pane" id="press">@include("learncenter.press")</div>
                        <div role="tabpanel" class="tab-pane" id="privacy">@include("learncenter.privacy")</div>
                        <div role="tabpanel" class="tab-pane" id="terms">@include("learncenter.terms")</div>
                        <div role="tabpanel" class="tab-pane" id="contact">@include("learncenter.contact")</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if (!Auth::check())
        <div class="space-lg"></div>
        <div class="signup-banner">

            <center>

                <h2 class="extralight">Sign up during beta for a Premium Account <span
                            class="text-color2">FREE FOR LIFE!</span>
                </h2>

                <div class="space-md"></div>
                <a href="{{URL::action("UserController@getRegister")}}">
                    <div class="button-home button-home-color2">Join Now!</div>
                </a>
            </center>

        </div>
        <div class="space-lg hidden-xs"></div>
    @endif
@stop

