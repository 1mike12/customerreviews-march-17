
<div class="testimonials-wrapper">
    <h2>Testimonials</h2>
  <div class="space-sm"></div> 
  
    <div class="text-center">
      
            Thanks to Clientype, I worry less about doing business with clients who could hurt my business.
            <br/>
            Kevin, Finish Carpenter | MA
      
        <div class="space-md"></div>
        <div class="space-md"></div>
    
            I am glad a website like this is available to small businesses like ours that rely on fair business. 
            <br/>
            John, Mover | MA
    
        <div class="space-md"></div>
        <div class="space-md"></div>
        
     
            After the number of clients that are a negative impact to our business, I will be checking {{COMPANY}} regularly.
            <br/>
            Kenny, Locksmith | MA
   
        <div class="space-md"></div>
        <div class="space-md"></div>

            Most business is good business but that small fraction can really set a business like ours back.
            <br/>
            Stephen, Plumber | MA
    
         <div class="space-md"></div>
        <div class="space-md"></div>
        
          It is refreshing to know that there is a way to help prevent nasty clients!
            <br/>
            Jennifer, Caterer | MA
        
        
    </div>
</div>
