
<div class="founders-wrapper"> 
    <h2>Our Co-Founders</h2>
    <div class="space-sm"></div> 

    <div class="panel panel-default">
        <div class="panel-body">

            <div class="row">
                <div class="col-sm-4">
                    <center>
                        <img src="imagesFounders/founder-mike.jpg" alt="Founder Image" class="img-circle">
                    </center>
                    <div class="space-sm visible-xs hidden-sm hidden-md hidden-lg"></div>  
                </div>
                <div class="col-sm-6">

                    <h3>Mike Qin</h3>
                    <div class="space-xs"></div> 
                    <h3><small>CTO & Co-Founder</small></h3>


                    <div class="space-sm"></div> 

                    <p class="line-height-p">
                        Mike is the co-founder and CTO at Clientype. He oversees the technical
                        strategy of the company, and is dedicated to building a team of world-class
                        engineers to keep Clientype at the forefront of the industry. Mike became
                        an entrepreneur early on running multiple businesses while he was in college.
                        He received a degree in Business Management from Bentley University and held
                        several engineering positions before becoming a co-founder at Clientype.
                    </p>
                </div>
            </div>   

            <div class="space-md"></div> 

            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-4">

                            <center>
                                <img src="imagesFounders/founder-brian.jpg" alt="Founder Image" class="img-circle">
                            </center>
                            <div class="space-sm visible-xs hidden-sm hidden-md hidden-lg"></div> 
                        </div>
                        <div class="col-sm-6">
                            <h3>Brian Gioia </h3>
                            <div class="space-xs"></div>
                            <h3><small>CEO & Co-Founder</small> </h3>

                            <div class="space-sm"></div> 

                            <p class="line-height-p">
                                Brian is the co-founder and CEO at Clientype. He drives the
                                company's vision, strategy and growth as it provides ways for
                                businesses to better manage their clients and client related decisions.
                                Brian leads the product team in creating meaningful features
                                through intuitive design, and oversees Clientype’s brand and product
                                development. He received a degree in Business Management at the Peter
                                T. Paul College of Business and Economics. Under Brian's leadership,
                                Clientype stands at the forefront of the business applications industry, and
                                is rapidly expanding.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="space-md"></div> 
        </div> 
    </div>
</div>


