
<div class="contact-wrapper">
    <h2>Contact Us</h2>
      <div class="space-sm"></div> 
      
 <div class="no-gutter">
    <div class="col-sm-6">
           <div class="panel panel-default">
            <div class="panel-body">
                <p><span class="glyphicon glyphicon-earphone"></span>&nbsp;&nbsp;603.401.1987</p>
                <p><span class="glyphicon glyphicon-envelope"></span>&nbsp;&nbsp;{{COMPANY}}@email.com</p>
                <p><span class="glyphicon glyphicon-map-marker"></span>&nbsp;&nbsp;UPS PO Box in Boston</p>
            </div>
        </div>
    </div>       
</div>
</div>
