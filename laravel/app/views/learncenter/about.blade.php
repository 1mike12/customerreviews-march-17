
<div class="about-wrapper">
    <h2>About Us</h2>
    
    
    
    
    <div style="img{height: 0px !important;}">
  <iframe width='100%' height='520' frameborder='0' src='http://bgioia1.cartodb.com/viz/f221f754-c094-11e4-8c3a-0e4fddd5de28/embed_map' allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe>
    
     <div style="
         width: 100%;
          height: 80px;
          background: white;
          position: absolute;
          z-index: 99;
          bottom: 40px;
          "></div>
    </div>
    
    
    
    
    
    
    <div class="row section-first">
        <div class="col-sm-9">
            <h3>Why we exist</h3>
            <p>
                {{COMPANY}} is the highest trafficked site for quickly researching
                and rating customers. {{COMPANY}} allows
                businesses to get to know there clients before doing business with
                them. {{COMPANY}} is for primarily service companies who have the
                luxury of turning down business with harmful clients. Our goal is to
                help protect businesses from losing money due to harmful customers.
            </p>
        </div>
        <div class="col-sm-3 hidden-xs visible-sm visible-md visible-lg"> 
            <span class="pull-right glyphicon fa fa-search"></span>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8 line-grey"></div>
    </div>

    <div class="row section">
        <div class="col-sm-3 hidden-xs visible-sm visible-md visible-lg">
            <span class="col-sm-offset-1 pull-left glyphicon fa fa-usd"></span>
        </div>
        <div class="col-sm-9">
            <h3>Completely free</h3>
            <p>
                Our system is completely free for businesses.
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-offset-4 col-sm-8 line-grey"></div>
    </div>

    <div class="row section">
        <div class="col-sm-9">
            <h3>Account privacy</h3>
            <p>
                Our first priority is keeping all account information private.
                All reviews are added to the site at a random date within
                a given time frame allowing you to review someone immediately
                after a job. For mor information see the <a>privacy policy</a> page.
            </p>
        </div>
        <div class="col-sm-3 hidden-xs visible-sm visible-md visible-lg">
            <span class="pull-right glyphicon fa fa-eye-slash"></span>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8 line-grey"></div>
    </div>

    <div class="row section">
        <div class="col-sm-3 hidden-xs visible-sm visible-md visible-lg">
            <span class="col-sm-offset-1 pull-left glyphicon fa fa-star"></span>
        </div>
        <div class="col-sm-9">
            <h3>Quality reviews</h3>
            <p>
                Every day {{COMPANY}} receives ratings by individuals all across
                America. In addition to an advanced filtering system's
                {{COMPANY}} allows for users to flag any questionable review that our
                system may not have detected. Reviews are given on clients
                evaluating the timeliness of payment, ease of working with, general
                kindness as well as a detailed written explanation of the experience.
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-offset-4 col-sm-8 line-grey"></div>
    </div>

    <div class="row section">
        <div class="col-sm-9">
            <h2>Join our community</h2>
            <p>
                What does a freelance web developer, carpenter and landscape designer
                all have in common? They all have the ability of turning away harmful
                clients. Join our like-minded community of hard working individuals who
                deserve clients that respect and pay them and share a report on those
                who do not.
            </p>
        </div>
        <div class="col-sm-3 hidden-xs visible-sm visible-md visible-lg">
            <span class="pull-right glyphicon fa fa-globe"></span>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8 line-grey"></div>
    </div>

    <div class="row section">
        <div class="col-xs-3 hidden-xs">     
            <span class="pull-left glyphicon glyphicon-user user-2"></span>
            <span class="pull-left glyphicon glyphicon-user user-1"></span>
        </div>
        <div class="col-sm-9">
            <h3>Developers</h3>
            <p>
                {{COMPANY}} was made by two friends, both students of business at the time.
                Both self taught web developers and driven to solve the problem of having
                harmful clients.
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-offset-4 col-sm-8 line-grey"></div>
    </div>

    <div class="row section">
        <div class="col-sm-9 section-last">
            <h3>Future goals</h3>
            <p>
                {{COMPANY}} has many exiting new features in the works, furthering the
                depth and accuracy of our database. {{COMPANY}} is still in its early
                stages of <i>Version 1(beta)</i>.
            </p>
        </div>
        <div class="col-sm-3 hidden-xs visible-sm visible-md visible-lg">
            <span class="pull-right glyphicon fa fa-road"></span>
        </div>
    </div>
</div>



