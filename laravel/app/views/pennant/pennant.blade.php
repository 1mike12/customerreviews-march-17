<div class="hidden-xs">
    <div class="pennant-band non-printable">
        <div class="belt container-fluid">
            <div class="pull-left nav pennant-tab">

                @if (!Auth::check())
                    @if (Route::current()->getUri() !== "/")
                        <a href="{{URL::to("/")}}">
                            <div class="<?php if (Route::current()->getUri() === "dash") echo "pennant-current " ?>pennant-box pennant-box-1">
                                <span class="active-link">Home</span></div>
                        </a>
                    @endif
                @endif


                @if (Auth::check())
                    <a href="{{URL::to("/")}}">
                        <div class="<?php if (Route::current()->getUri() === "dash") echo "pennant-current " ?>pennant-box pennant-box-1">
                            <span class="active-link">Dashboard</span></div>
                    </a>
                @endif

                <a href="{{URL::to("blog")}}">
                    <div class="<?php if (Route::current()->getUri() === "blog") echo "pennant-current " ?>pennant-box">
                        <span>Discover</span></div>
                </a>

                <a href="{{URL::to("learncenter")}}">
                    <div class="<?php if (Route::current()->getUri() === "learncenter") echo "pennant-current " ?>pennant-box">
                        <span>Learn Center</span></div>
                </a>

                @if (Auth::check())
                    <a href="{{URL::to("settings")}}">
                        <div class="<?php if (Route::current()->getUri() === "account" || Route::current()->getUri() === "settings") echo "pennant-current " ?>pennant-box">
                            <span>Account</span></div>
                    </a>
                @endif

                @if (Route::current()->getUri() === "customer/*")
                    <a>
                        <div class="<?php if (Route::current()->getUri() === "customer/*") echo "pennant-current " ?>pennant-box">
                            <span>customer</span></div>
                    </a>
                @endif

                @if (Auth::check())
                    @if (Route::current()->getUri() === "dash")
                        <a class="collapsed hidden-xs" data-toggle="collapse" data-parent="#accordion"
                           href="#collapseHelp" aria-expanded="false" aria-controls="collapseHelp">
                            <div class="<?php if (Route::current()->getUri() === "#") echo "pennant-current " ?>pennant-box">
                                <span>Search Tips</span></div>
                        </a>
                    @endif
                @endif

            </div>

            <ul class="pull-right pennant-external-link hidden-xs">
                <div class="pull-right pointer">
                    <a href="https://www.facebook.com/clientype">
                        <li><i class="fa fa-facebook"></i></li>
                    </a>
                    <a href="#">
                        <li><i class="fa fa-twitter"></i></li>
                    </a>
                    <a href="https://www.linkedin.com/profile/guided?trk=registration&o=reg">
                        <li><i class="fa fa-linkedin"></i></li>
                    </a>
                    <a href="https://plus.google.com/u/0/b/109051554622987511820/dashboard/overview">
                        <li><i class="fa fa-google-plus"></i></li>
                    </a>
                    <a href="https://www.youtube.com/channel/UCD1GzxRl8jb2KMkIQDlRgBw">
                        <li><i class="fa fa-youtube"></i></li>
                    </a>
                    <a href="https://www.pinterest.com/clientype/">
                        <li><i class="fa fa-pinterest"></i></li>
                    </a>
                </div>
            </ul>
        </div>
    </div>
</div>




