<div class="dash-wrapper">
    <div class="search-wrapper">
        <div class="search-box">
            <div class="icon-pattern">
                <div class="belt container-fluid form-group">

                    @if (Route::current()->getUri() === "dash")
                        <div class="label-height hidden-sm hidden-xs">
                           
                            <!--
                            <h1 class="text-center"><span class="opacity7">Search.&nbsp;&nbsp;Review.&nbsp;&nbsp;Improve.</span>
                            </h1>
                            -->
                            <div class="space-lg"></div>
                            
                        </div>
                    @endif

                    <center>
                        <div class="search-height">
                            <div class="no-gutter row dash-search-board">

                                {{ Form::open(["action"=>"SearchController@getSearch", 'class'=>'form form-inline', "method"=>"GET"]) }}
                                <div class="col-xs-5">
                                    <!--
                                         <div  tabindex="0" data-placement="top" role="button" data-toggle="popover"
                                               aria-hidden="false" data-trigger="focus"
                                               data-content="Invalid entry, try typing better">
                                         </div>
                                  -->
                                    <div class="search-banner-form1 col-sx-12">{{Form::text("name", null, ["class"=>"dash-search-box", "placeholder"=>"last name"])}}</div>
                                </div>

                                <div class="col-xs-5">
                                    <!--
                                        <div  tabindex="0" data-placement="top" role="button" data-toggle="popover"
                                              aria-hidden="false" data-trigger="focus"
                                              data-content="Invalid entry, try typing better">  
                                        </div>
                               -->
                                    <div class="search-banner-form2 col-xs-12">{{Form::text("location", null, ["class"=>"dash-search-box", "placeholder"=>"zip or address"])}}</div>
                                </div>


                                <div class="col-xs-2">
                                    <div class="search-banner-button">
                                        <button class="dash-search-button btn" type="submit"><i
                                                    class="glyphicon glyphicon-search"></i></button>
                                    </div>
                                </div>
                                {{Form::close()}}
                            </div>
                        </div>
                    </center>
                </div>
            </div>
        </div>
    </div>
</div>
