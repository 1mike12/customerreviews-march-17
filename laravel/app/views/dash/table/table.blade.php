<?php $controllerName = $resourcePath . "_ctrl" ?>
<div ng-controller="CustomerListController as {{$controllerName}}"
     controller-init="{{$resourcePath}}">
    <div ng-show="{{$controllerName}}.CLInstance.ready">
        @include("dash.table.tableHeader")
        <div class="space-sm"></div>

        <div ng-if="{{$controllerName}}.customerCount()>0">
            @include("dash.table.row")
        </div>
        <div ng-if="!({{$controllerName}}.customerCount()>0)">
            @include("result.noresult")
        </div>
    </div>
    <div ng-hide="{{$controllerName}}.CLInstance.ready">

        <div class="text-center text-colo1">
            <div class="space-md"></div>
            <i class="fa fa-spinner fa-spin fa-2x"></i>
            <div class="space-md"></div>
            
            <div class="space-xl"></div>
            <div class="space-xl"></div>
            
        </div>
    </div>
</div>