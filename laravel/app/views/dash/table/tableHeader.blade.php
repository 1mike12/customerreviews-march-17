<div class="hidden-xs">
    <div class="space-xs"></div>

    <div class="inline-block space-horizontal-md"></div>

    <div class="inline-block">
        <div class="text-center" ng-click="{{$controllerName}}.changeCompareOrder('overallAvg')">
            <div class="pointer"><i class="fa fa-sort-up"></i><br/>51<br/><i class="fa fa-sort-down"></i></div>
        </div>
    </div>

    <div class="inline-block space-horizontal-md"></div>

    <div class="inline-block">
        <div class="text-center" ng-click="{{$controllerName}}.changeCompareOrder('last_name')">
            <div class="pointer"><i class="fa fa-sort-up"></i><br/>AZ<br/><i class="fa fa-sort-down"></i></div>
        </div>
    </div>

    <div class="inline-block space-horizontal-md"></div>

    <input style="width: 200px;" placeholder="Search" type="text" class="form-control inline-block"
           ng-model="{{$controllerName}}.filter">


</div>
