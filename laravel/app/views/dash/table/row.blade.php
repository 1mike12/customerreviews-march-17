<div class="container-fluid">
    <div ng-repeat="customer in {{$controllerName}}.CLInstance.customers
                    | filter:{{$controllerName}}.filter
                                        | orderBy:{{$controllerName}}.order:{{$controllerName}}.orderReverse"
            >

        <div class="kill-result">
            <div class="row text-uppercase">
                <div class="col-sm-2 text-center pull-left">
                    <div ng-if="customer.reviewCount > 0">

                        <div class="pull-left inline-block space-horizontal-sm hidden-xs"></div>

                        <div class="pull-left">
                            <i ng-class="{'glyphicon-thumbs-up': customer.overallAvg >= 3, 'glyphicon-thumbs-down': customer.overallAvg < 3}"
                               class="glyphicon">

                            </i>
                        </div>

                        <div class="inline-block space-horizontal-sm"></div>

                        <label class="ultrabold font-size-md">
                            @{{customer.overallAvg}}
                        </label>

                        <a href="customer/@{{customer.id}}">
                            <i class="fa fa-star-o"></i>
                            <small class="sr7-text underline">
                                @{{customer.reviewCount}}
                            </small>
                        </a>

                        <div class="inline-block space-horizontal-sm"></div>
                    </div>

                    <div ng-if="!(customer.reviewCount > 0)">
                        <span>-</span>
                    </div>
                    <div class="space-sm"></div>
                </div>

                <div class="col-sm-2">
                    <div class="enlarge">
                        <div>
                            <a href="customer/@{{customer.id}}">
                                <img ng-src="@{{customer.streetViewURL}}" alt="Error" class="sr-house img-rounded">
                            </a>
                            <span><img ng-src="@{{customer.streetViewURL}}" class="img-rounded" width="300px"
                                       height="180px"
                                       alt="House"/></span>
                        </div>
                    </div>
                    <div class="space-sm"></div>
                </div>

                <div class="col-sm-2 pull-left">
                    <a href="customer/@{{customer.id}}">
                        <span class="underline color-text">
                            <strong>@{{customer.last_name}}</strong>
                            <br/>

                            @{{customer.first1}}
                            <i ng-if="customer.first2 !== ''">&nbsp;/&nbsp;</i>
                            @{{customer.first2}}

                        </span>
                    </a>

                    <div class="space-sm"></div>
                </div>

                <div class="col-sm-5">
                    <div class="row">
                        <div class="col-sm-6 pull-left">
                            <a href="customer/@{{customer.id}}">
                                <span class="underline color-text">
                                    @{{customer.address1}}, @{{customer.address2}}<br>
                                    @{{customer.city}}, @{{customer.state}} @{{customer.zip}}
                                </span>
                            </a>

                            <div class="space-sm"></div>
                        </div>
                        <div class="col-sm-6">
                            <input
                                    type="text"
                                    class="form-control"
                                    placeholder="Notes..."
                                    ng-model="customer.pivot.note"
                                    ng-focus="customer.focus()"
                                    ng-blur="customer.blur()"
                                    >

                            <div class="space-sm"></div>
                        </div>

                    </div>
                </div>

                <div class="hidden-xs">
                    <div class="col-sm-1">
                        <div class="space-xs"></div>
                        <div class="space-xs"></div>
                        <i style="cursor:pointer;" class="kill-result-target fa fa-times"
                           ng-click="{{$controllerName}}.CLInstance.destroyCustomer(customer)"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>