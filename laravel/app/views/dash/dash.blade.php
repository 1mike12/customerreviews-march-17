
<div class="dash-wrapper">
    <div class="belt-none-xs">

        <div class="container-fluid">
            <div class="row">

                    <div class="col-md-9">
                        <div class="page-background">
                            <!-- =========== tabs =========== -->
                            <ul class="dash-tabs nav nav-tabs" role="tablist">
                                <li class="active">
                                    <a href="#my-clients" role="tab" data-toggle="tab">
                                        <span class="tab-text tab-clients">My Clients</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="#recently-viewed" role="tab" data-toggle="tab">
                                        <span class="tab-text tab-clients">Recently Viewed</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#review-history" role="tab" data-toggle="tab">
                                        <span class="tab-text tab-clients">Review History</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#top-clients" role="tab" data-toggle="tab">
                                        <span class="tab-text tab-best">Top Clients&nbsp;&nbsp;<i
                                                class="fa fa-thumbs-up"></i></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#worst-offenders" role="tab" data-toggle="tab">
                                        <span class="tab-text tab-worst">Worst Offenders&nbsp;&nbsp;<i class="fa fa-thumbs-down"></i>
                                                <!-- &nbsp;<span class="tab-seen label label-danger">New</span> -->
                                        </span>
                                    </a>
                                </li>
                            </ul>


                            <!-- Tab panes ======== -->
                            <div class="tab-content panel panel-default">

                                <div class="tab-pane fade in active" id="my-clients">
                                    @include("dash.table.table", ["resourcePath"=>"my_customers"])
                                </div>

                                <div class="tab-pane fade" id="recently-viewed">
                                    @include("dash.table.table", ["resourcePath"=>"recent_customers"])
                                </div>

                                <div class="tab-pane fade" id="review-history">

                                    <!-- =========== delete =========== -->
                                    <div class="text-center text-colo1">
                                        <div class="space-md"></div>
                                        <i class="fa fa-spinner fa-spin fa-2x"></i>
                                        <div class="space-md"></div>

                                        <div class="space-xl"></div>
                                        <div class="space-xl"></div>

                                    </div>
                                    <!-- =========== delete =========== -->


                                </div>

                                <div class="tab-pane fade" id="top-clients">
                                    @include("dash.table.table", ["resourcePath"=>"best_customers"])
                                </div>

                                <div class="tab-pane fade" id="worst-offenders">
                                    @include("dash.table.table", ["resourcePath"=>"worst_customers"])
                                </div>
                            </div>
                        </div>
                    </div>
             

                <!-- sidebar -->
                <div class="col-md-3">
                    @include("dash.sidebar")
                </div>


            </div>
        </div>
    </div>
</div>


