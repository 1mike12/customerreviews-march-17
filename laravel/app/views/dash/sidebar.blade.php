
<div class="sidebar-wrapper">
      <div class="join-us-box col-xs-12 sideBar-info-box text-effect-1 hidden-lg hidden-md">
        <span>Join us on</span>
        <br>
        <ul class="col-xs-12">
            <li><a href="http://facebook.com/"> <i class="fa fa-facebook-square"></i> </a></li>
            <li><a href="http://twitter.com/"> <i class="fa fa-twitter-square"></i> </a></li>
            <li><a href="http://linkedin.com/"> <i class="fa fa-linkedin-square"></i> </a></li>
            <li><a href="http://plus.google.com/"> <i class="fa fa-google-plus-square"> </i></a></li>
        </ul>
    </div>
    
       <div class="col-xs-12 sideBar-info-box text-effect-1 hidden-xs">
        <span>
            <script language="JavaScript">
                var stickyNoteText = [
                    "We are sure you have had great clients in the past, give them the reviews they deserve!",
                    "Thanks to your honest reviews {{COMPANY}} is the largest customer review site on the internet.",
                    "Not a lot of reviews in your home town? Hang in there, {{COMPANY}} was launched only <strong>3</strong> days ago.",
                    "The average user has reviewed <strong>6</strong> people! Thank you for standing up to bad clients!",
                    "Stand up against bad clients everywhere!"
                ];
                var i = Math.floor(5 * Math.random());
                document.write(stickyNoteText[i]);
            </script>     
        </span>
</div>
    
    <div class="col-xs-12 sideBar-info-box text-effect-1">
        <span>Upload a Client List</span>
        <br>Easily organize your clients all on your dashboard.
    </div>
    
    <div class="col-xs-12 sideBar-info-box text-effect-1 hidden-xs">
        <span>{{COMPANY}} is portable</span>
        <br>Need to search on the go? Try {{COMPANY}} with a mobile device!
    </div>
    
    
    
    
</div>


