@extends("layouts.noHeadernoFooter")

@section("content")

<div class="the-404-wrapper">
    <div class="space-lg"></div>

    <!-- Inspiration: http://drbl.in/nOzJ -->
    <h1 class="ultrabold">404 Error</h1>
    <h3>Page not found</h3>

    <div class="sea">
        <div class="circle-wrapper">
            <div class="bubble"></div>
            <div class="submarine-wrapper">
                <div class="submarine-body">
                    <div class="window"></div>
                    <div class="engine"></div>
                    <div class="light"></div>
                </div>
                <div class="helix"></div>
                <div class="hat">
                    <div class="leds-wrapper">
                        <div class="periscope"></div>
                        <div class="leds"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="space-md"></div>

    <center>                       
        <h4><a class="bold the-404-link" href="{{URL::to("/")}}">Dashboard</a></h4>
    </center>
</div>

@stop