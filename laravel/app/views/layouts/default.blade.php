@include("layouts.partials.head")
<body class=" <?php if (Route::current()->getUri() === "/") echo "home" ?> " ng-app="myApp">

@include("dash.help")
@include("layouts.partials.header")

<div class="content-wrapper">
    @include("modal.login")
    @include("errors.alerts")

    @if (Route::current()->getUri() === "dash")
        @include("dash.searchBanner")
    @endif

    @if (Route::current()->getUri() === "search")
        @include("dash.searchBanner")
    @endif

    @if (Route::current()->getUri() !== "/")
        @include("pennant.pennant")
    @endif

    @yield("content")

</div>
@include("layouts.partials.footer")
@include("layouts.partials.scripts")
</body>


