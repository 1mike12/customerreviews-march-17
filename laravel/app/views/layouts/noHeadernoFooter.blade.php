@include("layouts.partials.head")
<body>
    @yield("content")
    @include("layouts.partials.scripts")
</body>


