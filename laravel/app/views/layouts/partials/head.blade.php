<head>
    <base href="/customerreviews/laravel/public/" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400' rel='stylesheet' type='text/css'>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- to ensure proper rendering and touch zooming -->
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900,200italic,300italic,400italic,600italic,700italic,900italic'
          rel='stylesheet' type='text/css'>

    <!-- (?) I need to find out why this is needed for BS icons -->
     <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

    <!-- (?) tooltips not working with this link -->
    <!-- our own bootstrap compiled -->
    {{HTML::style("css/bootstrap.css")}}
    <!-- style sheets -->
    {{HTML::style("css/concatenated.css")}}
</head>