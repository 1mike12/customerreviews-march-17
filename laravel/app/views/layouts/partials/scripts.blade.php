<script>
    var PUBLIC = "{{URL::to('/') . '/'}}";
</script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
{{--angular included locally (for code completion)--}}
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script src="scripts/sweet-alert.min.js"></script>

<?php
//including all files in /scripts
$files = File::allFiles("scripts");
$acceptableExtensions = ["js"];
$paths = [];
foreach ($files as $file) {
    $extension = strtolower($file->getExtension());
    if (in_array($extension, $acceptableExtensions)) {
        $paths[] = $file->getPathname();
    }
}
?>
@foreach($paths as $path)
    {{HTML::script($path)}}
@endforeach
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.14/angular-route.js"></script>

<?php
//including all files in /angular
$files = File::allFiles("angular");
$acceptableExtensions = ["js"];
$paths = [];
foreach ($files as $file) {
    $extension = strtolower($file->getExtension());
    if (in_array($extension, $acceptableExtensions)) {
        $paths[] = $file->getPathname();
    }
}
?>
@foreach($paths as $path)
    {{HTML::script($path)}}
@endforeach

