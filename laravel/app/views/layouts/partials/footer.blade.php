
@if (Route::current()->getUri() !== "/")
<div class="space-md background-color"></div>
@endif

<div class="no-gutter footer-wrapper non-printable hidden-xs">
<div class="container-fluid">
    <div class="footer-links">


        <div class="col-sm-3"></div>
        <div class="col-sm-2">

            <ul class="nav-stacked">
                <li role="presentation"> <h5>Company</h5></li>
                <li role="presentation"><a href="{{URL::to("learncenter")}}">Find an Answer</a></li>
                <li role="presentation"><a href="{{URL::to("learncenter")}}">Testimonials</a></li>
                <li role="presentation"><a href="{{URL::to("learncenter")}}">Founders</a></li>
                <li role="presentation"><a href="{{URL::to("learncenter")}}">Press</a></li>
                <li role="presentation"><a href="{{URL::to("learncenter")}}">Privacy Policy</a></li>
                <li role="presentation"><a href="{{URL::to("learncenter")}}">Terms</a></li>
                <li role="presentation"><a href="{{URL::to("learncenter")}}">Contact Us</a></li>
            </ul>   
        </div>

        <div class="col-sm-2">
            <ul class="nav-stacked">
                <li role="presentation"><h5>Discover</h5></li>
                <li role="presentation"><a href="{{URL::to("blog")}}">Blog</a></li>
                <li role="presentation"><a href="{{URL::to("learncenter")}}">Clients By Location</a></li>
                <li role="presentation"><a href="{{URL::to("learncenter")}}">Site Map</a></li>
            </ul>   
        </div>

        <div class="col-sm-2">
            <ul class="nav-stacked">
                <li role="presentation"><h5>Join Us On</h5></li>
                <li role="presentation"><a href="#">Linked In</a></li>
                <li role="presentation"><a href="https://plus.google.com/u/0/b/109051554622987511820/dashboard/overview">Google+</a></li>
                <li role="presentation"><a href="#">Twitter</a></li>
                <li role="presentation"><a href="#">Facebook</a></li>
                <li role="presentation"><a href="https://www.youtube.com/channel/UCD1GzxRl8jb2KMkIQDlRgBw">Youtube</a></li>
                <li role="presentation"><a href="https://www.pinterest.com/clientype/">Pinterest</a></li>
            </ul>   
        </div>

    </div>
    </div>

    <div class="container-fluid">
        <hr class="col-sm-offset-3 col-sm-6" />
    </div>

    <center>
        <div>&copy;&nbsp; {{COMPANY}}, Inc.</div>
    </center>

</div>
