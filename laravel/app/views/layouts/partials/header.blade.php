<div class="hidden-xs">
    <?php
    $headerSearchRoutes = ["customer", "demo"];
    ?>

    <div class="non-printable">
        <div id="header" class="
    <?php if (Route::current()->getUri() === "/") echo "unScrolled"; ?>
    <?php if (Route::current()->getUri() === "/") echo "header-fixed"; ?>
         ">
            <div class="belt container-fluid">

                <div class="space-header-sm"></div>


                <div>
                    @if (Route::current()->getUri() === "/")
                        <a class="pull-left" href="{{URL::to("/")}}">
                            <img src="{{URL::asset("images/logo-header-before.png")}}" alt="Error"
                                 class="logo-header-before">
                        </a>
                    @endif


                    <a class="pull-left" href="{{URL::to("/")}}">
                        <img src="{{URL::asset("images/logoheader.png")}}" alt="Error" class="logo-header">
                    </a>

                </div>
                <ul class="homenav">
                    @if (Auth::check())

                        <div class="dropdown">
                            <!--modal include-->
                            <!-- feeding customer id variable into model -->
                            @include("modal.review")

                            <button class="nav0 btn btn-default" type="button" id="dropSet" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                {{Auth::user()->company_name}}
                                <span class="caret"></span>
                            </button>
                            <ul class="pull-right dropdown-menu dropdown-menu-dash" role="menu"
                                aria-labelledby="dropSet">
                                <li role="presentation" class="dropdown-header">Default
                                    zip:&nbsp;{{Auth::user()->default_zip}}</li>
                                <li role="presentation" class="dropdown-header">Reviews given:&nbsp;6</li>
                                <li role="presentation" class="divider"></li>
                                <li role="presentation"><a href="{{URL::to("settings")}}" class="menuitem-band"
                                                           role="menuitem" tabindex="-1">Settings
                                        <div class="pull-right"><i style="line-height:20px;" class="fa fa-pencil"></i>
                                        </div>
                                    </a></li>
                                <li role="presentation"><a href="{{URL::to("learncenter")}}" class="menuitem-band"
                                                           role="menuitem" tabindex="-1">Help Center
                                        <div class="pull-right"><i style="line-height:20px;" class="fa fa-question"></i>
                                        </div>
                                    </a></li>
                                <li role="presentation" class="divider"></li>
                                <li role="presentation"><a id="signout-alert" class="menuitem-band" role="menuitem"
                                                           tabindex="-1"
                                                           href="{{URL::action("UserController@getSignout")}}">Log out
                                        <div class="pull-right"><i style="line-height:20px;"
                                                                   class="fa fa-power-off"></i></div>
                                    </a></li>
                            </ul>
                        </div>

                        <a href="{{URL::to("/")}}">
                            <li class="header-nav-dashboard nav0 homenav1 hidden-xs visible-sm visible-md visible-lg">
                                Dashboard
                            </li>
                        </a>
                        </a>

                        <a href="{{URL::to("addCustomer")}}">
                            <li class="header-nav-dashboard header-nav-review bold">Add Client</li>
                        </a>
                        </a>

                    @else



                        <li data-toggle="modal" data-target="#login-target"
                            class="pointer homenav0 homenav-review homenav2 home-white hidden-xs visible-sm visible-md visible-lg">
                            Review a client
                        </li>

                        <div class="dropdown">

                            <li class="homenav0 homenav1 home-white hidden-xs" type="button" id="dropHome"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="pointer"> Help
                            <span class="caret"></span>
                        </span>
                            </li>

                            <ul class="pull-right dropdown-menu" role="menu" aria-labelledby="dropHome">
                                <li><a href="{{URL::to("learncenter")}}"><span class="underline text-color2">Visit the Learn Center</span></a>
                                </li>
                                <li><a href="{{URL::to("learncenter")}}">Where can I learn about {{COMPANY}}?</a></li>
                                <li><a href="{{URL::to("learncenter")}}">How do I search for a client?</a></li>
                                <li><a href="{{URL::to("learncenter")}}">How do I review a client?</a></li>
                                <li><a href="{{URL::to("learncenter")}}">How do I remain anonymous?</a></li>
                                <li><a href="{{URL::to("learncenter")}}">See all FAQs</a></li>
                            </ul>
                        </div>

                        <li data-toggle="modal" data-target="#login-target"
                            class="pointer homenav0 homenav1 home-white hidden-xs visible-sm visible-md visible-lg">
                            Login
                        </li>

                        <a href="{{URL::action("UserController@getRegister")}}">
                            <li class="homenav0 homenav1 home-white hidden-xs visible-sm visible-md visible-lg">Sign
                                up
                            </li>
                        </a>




                    @endif
                </ul>
            </div>
        </div>
    </div>
</div>


