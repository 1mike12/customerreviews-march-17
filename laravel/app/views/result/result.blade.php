<div class="row text-uppercase">

    <!--thumbs up down-->
    <div class="col-sm-2 text-center">
        @if ($customer->hasReviews)
            @if($customer->overallAvg>=3.0)
                <span class="glyphicon glyphicon-thumbs-up"></span><br>
            @else
                <span class="glyphicon glyphicon-thumbs-down"></span><br>
            @endif
            <label class="ultrabold">{{$customer->overallAvg}}</label>
            <a href="{{URL::action("CustomerController@getCustomer", $customer->id)}}">
                <i class="fa fa-star-o"></i>
                <small class="sr7-text underline">
                    {{$customer->reviewCount}}
                </small>
            </a>
        @else
            <span>-</span>
        @endif
    </div>

    <div class="col-sm-2">
        <div class="enlarge">
            <div>
                <a href="{{URL::action("CustomerController@getCustomer", $customer->id)}}">
                    <img src="{{$customer->streetViewURL()}}" alt="Error" class="sr-house img-rounded">
                </a>
                <span><img src="{{$customer->streetViewURL()}}" class="img-rounded" width="300px" height="180px"
                           alt="House"/></span>
            </div>
        </div>
    </div>

    <div class="col-sm-2">
        <a href="{{URL::action("CustomerController@getCustomer", $customer->id)}}">
            <span class="underline color-text">
                <strong>{{$customer->last_name}}</strong>
                <br>
                {{$customer->first1}}
                @if($customer->first2 !="")
                    / {{$customer->first2}}
                @endif
            </span>
        </a>
    </div>

    <div class="col-sm-5">
        <div class="row">
            <div class="col-sm-6">
                <a href="{{URL::action("CustomerController@getCustomer", $customer->id)}}">
                    <span class="underline color-text">
                        {{$customer->address1}}, {{$customer->address2}}<br>
                        {{$customer->city}}, {{$customer->state}} {{$customer->zip}}
                    </span>
                </a>
            </div>
            <div class="col-sm-6">
                <input type="text" class="form-control" placeholder="Add note">
            </div>
        </div>
    </div>

    <div class="hidden-xs">
        <div class="col-sm-1">
            <a href="#">
                <i class="fa fa-trash-o fa-2x"></i>
            </a>
        </div>
    </div>
</div>
<div class="space-sm"></div>