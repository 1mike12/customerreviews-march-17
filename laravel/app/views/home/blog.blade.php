@extends("layouts.default")
@section("content")
    <?php
    $blog = array(
            array(
                    "title" => "ATLASDB: TRANSACTIONS FOR DISTRIBUTED KEY-VALUE STORES (PART I)",
                    "date" => "june 19, 2015",
                    "author" => "ari gesher",
                    "tag1" => '<div class="blog-tag text-center pointer"><span class="capitalize">business</span></div>',
                    "tag2" => '<div class="blog-tag text-center pointer"><span class="capitalize">clientpe data</span></div>',
                    "tag3" => '',
                    "tag4" => '',
                    "tag5" => '',
                    "image" => '<img class="blog-image" src="imagesBlog/blog-9337.png" alt="Image Error">',
                    "abstract" => "AtlasDB is a massively scalable datastore and transactional layer that can be placed on top of any key-value store to give it ACID properties. This is the first of several blog posts that will introduce AtlasDB and describe how we built it at Palantir.",
            ),
            array(
                    "title" => "Lorum Ipsom TRANSACTIONS FOR DISTRIBUTED KEY-VALUE STORES (PART I)",
                    "date" => "april 13, 2015",
                    "author" => "ben gesher",
                    "tag1" => '<div class="blog-tag text-center pointer"><span class="capitalize">on the job</span></div>',
                    "tag2" => '',
                    "tag3" => '',
                    "tag4" => '',
                    "tag5" => '',
                    "image" => '',
                    "abstract" => "Lorum Ipsom is a massively scalable datastore and transactional layer that can be placed on top of any key-value store to give it ACID properties. This is the first of several blog posts that will introduce AtlasDB and describe how we built it at Palantir.",
            ),
            array(
                    "title" => "somthing happened: TRANSACTIONS FOR DISTRIBUTED KEY-VALUE STORES (PART I)",
                    "date" => "june 19, 2015",
                    "author" => "ari gesher",
                    "tag1" => '<div class="blog-tag text-center pointer"><span class="capitalize">carpentry</span></div>',
                    "tag2" => '',
                    "tag3" => '',
                    "tag4" => '',
                    "tag5" => '',
                    "image" => '<img class="blog-image" src="imagesBlog/blog-2664.png" alt="Image Error">',
                    "abstract" => "AtlasDB is a massively scalable datastore and transactional layer that can be placed on top of any key-value store to give it ACID properties. This is the first of several blog posts that will introduce AtlasDB and describe how we built it at Palantir.",
            ),
            array(
                    "title" => "Lorum Ipsom TRANSACTIONS FOR DISTRIBUTED KEY-VALUE STORES (PART I)",
                    "date" => "june 22, 2014",
                    "author" => "john gesher",
                    "tag1" => '<div class="blog-tag text-center pointer"><span class="capitalize">on the job</span></div>',
                    "tag2" => '',
                    "tag3" => '',
                    "tag4" => '',
                    "tag5" => '',
                    "image" => '',
                    "abstract" => "Lorum Ipsom is a massively scalable datastore and transactional layer that can be placed on top of any key-value store to give it ACID properties. This is the first of several blog posts that will introduce AtlasDB and describe how we built it at Palantir.",
            ),
            array(
                    "title" => "somthing happened: TRANSACTIONS FOR DISTRIBUTED KEY-VALUE STORES (PART I)",
                    "date" => "june 2, 2014",
                    "author" => "ari gesher",
                    "tag1" => '<div class="blog-tag text-center pointer"><span class="capitalize">carpentry</span></div>',
                    "tag2" => '',
                    "tag3" => '',
                    "tag4" => '',
                    "tag5" => '',
                    "image" => '',
                    "abstract" => "AtlasDB is a massively scalable datastore and transactional layer that can be placed on top of any key-value store to give it ACID properties. This is the first of several blog posts that will introduce AtlasDB and describe how we built it at Palantir.",
            ),
    );
    ?>


    <div class="space-sm"></div>

    <div class="blog-wrapper">
        <div class="background-color2">

        </div>


        <div class="space-lg"></div>
        <div class="container-fluid">
            <div class="row belt">
                <div class="col-sm-offset-1 col-sm-8">

                    <?php foreach ($blog as $blog) { ?>

                    <!-- ============= title ============= -->
                    <h1 class="uppercase bold"><?php echo $blog["title"] ?></h1>


                    <!-- ============= info band ============= -->
                    <div class="space-sm"></div>
                    <i class="fa fa-clock-o"></i>&nbsp;
                    <span class="capitalize"><?php echo $blog["date"] ?></span>

                    &nbsp;&nbsp;&nbsp;&nbsp;

                    <i class="fa fa-user"></i>&nbsp;
                    <span class="capitalize"><?php echo $blog["author"] ?></span>

                    <br class="hidden-sm hidden-md hidden-lg"/>
                    <span class="hidden-xs">&nbsp;&nbsp;&nbsp;&nbsp;</span>

                    <i class="fa fa-tag"></i>&nbsp;
                    <?php echo $blog["tag1"] ?>
                    <?php echo $blog["tag2"] ?>
                    <?php echo $blog["tag3"] ?>
                    <?php echo $blog["tag4"] ?>
                    <?php echo $blog["tag5"] ?>

                    <div class="space-sm"></div>
                    <!-- ============= image ============= -->
                    <?php echo $blog["image"] ?>

                    <!-- ============= abstract ============= -->
                    <div class="space-sm"></div>
                    <p class="line-height-p"><?php echo $blog["abstract"] ?></p>


                    <div class="space-sm"></div>
                    <div class="space-sm"></div>
                    <div class="btn-blog uppercase normal text-center pointer">
                        continue reading&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-chevron-right"></i>
                    </div>

                    <div class="space-lg"></div>
                    <div class="space-md"></div>
                    <?php } ?>

                </div>
                <div class="col-sm-1"><!-- nothing --></div>
                <div class="col-sm-2">
                    Business
                    <br/>
                    tags
                </div>
            </div>
        </div>
    </div>
@stop

