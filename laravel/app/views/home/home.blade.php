<!--

   /$$$$$$  /$$ /$$                       /$$
  /$$__  $$| $$|__/                      | $$
 | $$  \__/| $$ /$$  /$$$$$$  /$$$$$$$  /$$$$$$   /$$   /$$  /$$$$$$   /$$$$$$
 | $$      | $$| $$ /$$__  $$| $$__  $$|_  $$_/  | $$  | $$ /$$__  $$ /$$__  $$
 | $$      | $$| $$| $$$$$$$$| $$  \ $$  | $$    | $$  | $$| $$  \ $$| $$$$$$$$
 | $$    $$| $$| $$| $$_____/| $$  | $$  | $$ /$$| $$  | $$| $$  | $$| $$_____/
 |  $$$$$$/| $$| $$|  $$$$$$$| $$  | $$  |  $$$$/|  $$$$$$$| $$$$$$$/|  $$$$$$$
  \______/ |__/|__/ \_______/|__/  |__/   \___/   \____  $$| $$____/  \_______/
                                                  /$$  | $$| $$
                                                 |  $$$$$$/| $$
                                                  \______/ |__/
-->

@extends("layouts.default")

@section("content")
    <!-- Home Video Modal -->
    <div class="modal fade" id="video-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">

        <div class="space-sm"></div>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <center>
                    <iframe src="http://www.youtube.com/embed/fRYG-000ZJg"
                            style="width: 100%; height: 90%;" frameborder="0" allowfullscreen>
                    </iframe>
                </center>

                <i style="color: gray; position: absolute; z-index: 4; top: 30px; right: -20px;"
                   data-toggle="modal" data-target="#video-modal" class="pointer fa-4x fa fa-times-circle"></i>

            </div>
        </div>
    </div>

    <div class="landing-wrapper">
        <div class="landing-underlay"></div>
        <video autoplay loop poster="none" class="video-loop">
            <source src="video/firstloop.mp4" type="video/mp4">
        </video>

        <div class="landing-overlay">
            <div class="space-xl"></div>

            <div class="belt">
                <center>
                    <h1 class="ultrabold">NOT ALL BUSINESS IS</h1>

                    <h1 class="ultrabold">GOOD FOR BUSINESS</h1>

                    <div class="space-sm"></div>
                    <h2 class="extralight">Clientype Lets You Search
                        <br class="hidden-sm hidden-md hidden-lg"/>
                        And Review Customers</h2>

                    <div class="space-md"></div>

                    <a href="{{URL::action("UserController@getRegister")}}">
                        <div class="inline-block pointer button-home button-home-color2">Get Started</div>
                    </a>

                    <div class="hidden-xs inline-block space-horizontal-md"></div>
                    <div class="space-sm hidden-sm hidden-md hidden-lg"></div>

                    <div class="inline-block pointer button-home button-home-color4" data-toggle="modal"
                         data-target="#login-target">Login
                    </div>
                    <br/>

                    <div class="space-md"></div>
                    <div class="space-sm"></div>
                    <div class="hidden-xs glyphicon glyphicon-play-circle pointer" data-toggle="modal"
                         data-target="#video-modal"></div>
                </center>
            </div>
        </div>


        <div class="space-overlay"></div>


        <div class="background-color2 hidden-xs">
            <div class="space-sm"></div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-offset-1 col-sm-2">
                        <center><img src="images/logo-1.png" alt="Image" class="img"></center>
                    </div>
                    <div class="col-sm-2">
                        <center><img src="images/logo-2.png" alt="Image" class="img"></center>
                    </div>
                    <div class="col-sm-2">
                        <center><img src="images/logo-3.png" alt="Image" class="img"></center>
                    </div>
                    <div class="col-sm-2">
                        <center><img src="images/logo-4.png" alt="Image" class="img"></center>
                    </div>
                    <div class="col-sm-2">
                        <center><img src="images/logo-5.png" alt="Image" class="img"></center>
                    </div>
                </div>
            </div>
            <div class="space-sm"></div>
        </div>
        <div class="line-grey"></div>

        <div class="space-lg"></div>

        <div class="belt">
            <center><h3 class="bold">Key Features</h3></center>
            <div class="space-sm"></div>
            <center><h2 class="light">Clientype is a <span class="bold text-color2">FREE</span> database of reviewed
                    clients</h2></center>
            <center><h2 class="light">organized by name and location</h2></center>

            <div class="space-md"></div>
            <div class="space-sm"></div>

            <div class="container-fluid">
                <div class="row">
                    <div class="space-sm"></div>
                    <div class="col-sm-4">
                        <center><i class="fa fa-search fa-2x"></i></center>
                        <div class="space-sm"></div>
                        <center><h3 class="semibold">Search</h3></center>
                        <center>
                            <h4 class="extralight">
                                All reviews are given by<br/>
                                verified businesses
                            </h4>
                        </center>
                    </div>

                    <div class="col-sm-4">
                        <div class="space-sm"></div>
                        <center><i class="fa fa-star fa-2x"></i></center>
                        <div class="space-sm"></div>
                        <center><h3 class="semibold">Review</h3></center>
                        <center>
                            <h4 class="extralight">
                                Provide your experiences<br/>
                                anonymously
                            </h4>
                        </center>
                    </div>

                    <div class="col-sm-4">
                        <div class="space-sm"></div>
                        <center><i class="fa fa-wrench fa-2x"></i></center>
                        <div class="space-sm"></div>
                        <center><h3 class="semibold">Improve</h3></center>
                        <center>
                            <h4 class="extralight">
                                Avoid harmful customers, saving<br/>
                                your business time and money
                            </h4>
                        </center>
                    </div>
                </div>
            </div>
        </div>

        <div class="space-sm"></div>
        <div class="space-xl"></div>


        <!-- Section -->
        <div class="line-color2"></div>
        <div class="background-color">
            <div class="space-md"></div>


            <!-- section -->

            <div class="container-fluid">
                <div class="testimonial">


                    <div id="testimonial" class="carousel slide" data-ride="carousel">

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <center><img src="images/testimonial-1.png" alt="Image" class="img-circle"></center>
                                <div class="space-sm"></div>
                                <center><h4 class="normal-it">- Jane, Landcape Designer | MA</h4></center>
                                <div class="space-sm"></div>
                                <div class="row">
                                    <div class="col-sm-offset-2 col-sm-8">
                                        <center><h3 class="light-it">
                                                Thanks to Clientype, I found a review of a potential client saying that
                                                they cheated their last two contractors out of payments and saved
                                                my business that hassle!
                                            </h3></center>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <center><img src="images/testimonial-2.jpg" alt="Image" class="img-circle"></center>
                                <div class="space-sm"></div>
                                <center><h4 class="normal-it">- Mike, Carpenter | MA</h4></center>
                                <div class="space-sm"></div>
                                <div class="row">
                                    <div class="col-sm-offset-2 col-sm-8">
                                        <center><h3 class="light-it">
                                                After showing not receiving a payment for over two
                                                months I told the client I wouldreport them on {{COMPANY}}
                                                and I was payed that day! I never do business now without
                                                looking up a client.
                                            </h3></center>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Controls -->
                        <div>
                            <a class="left carousel-control" href="#testimonial" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#testimonial" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>


            <div class="space-sm"></div>
            <div class="space-md"></div>
        </div>
        <div class="line-color2"></div>


        <div class="space-xl"></div>
        <div class="belt">
            <center><h1 class="semibold">You Are <span class="ultrabold">Completely Anonymous</span></h1></center>
            <div class="space-sm"></div>
            <center><h2 class="normal">Clientype uses state-of-the-art security measures<br class="hidden-xs"/>
                    when handling your information.</h2></center>
            <div class="space-sm"></div>
            <center>
                <h3 class="light">
                    Reviews are randomly posted within a given<br class="hidden-xs"/>
                    time frame after submission to further ensure<br class="hidden-xs"/>
                    your anonymity.
                    <a href="#" class="text-color2">Learn More</a>
                </h3>
            </center>
        </div>

        <div class="space-sm"></div>
        <div class="space-xl"></div>


        <!-- Section -->
        <div class="line-color2"></div>
        <div class="background-color">
            <div class="space-md"></div>
            <center><h2 class="normal">Reviews in
                    <br class="hidden-sm hidden-md hidden-lg"/>
                <span class="extralight text-color2">
                    <span class="hidden-xs">&nbsp;&nbsp;</span>
                    Massachusetts
                </span>
                </h2>
            </center>

            <div class="space-sm"></div>

            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-offset-2 col-sm-2">
                        <div class="space-sm"></div>
                        <center><h1 class="light text-color2"><span class="text-color2">3K</span></h1></center>
                        <div class="space-sm"></div>
                        <center><h4 class="semibold">Verified Businesses</h4></center>
                        <div class="space-xs"></div>
                        <center><h5 class="light">currently using {{COMPANY}}
                                <br class="hidden-sm hidden-md hidden-lg"/>
                                reviews</h5></center>
                    </div>
                    <div class="col-sm-2">
                        <div class="space-sm"></div>
                        <center><h1 class="light"><span class="text-color2">82%</span></h1></center>
                        <div class="space-sm"></div>
                        <center><h4 class="semibold">Positive Reviews</h4></center>
                        <div class="space-xs"></div>
                        <center><h5 class="light">are far more common
                                <br class="hidden-sm hidden-md hidden-lg"/>
                                then negative ones</h5></center>
                    </div>
                    <div class="col-sm-2">
                        <div class="space-sm"></div>
                        <center><h1 class="light text-color2"><span class="text-color2">10K</span></h1></center>
                        <div class="space-sm"></div>
                        <center><h4 class="semibold">Reviews</h4></center>
                        <div class="space-xs"></div>
                        <center><h5 class="light">have already been added
                                <br class="hidden-sm hidden-md hidden-lg"/>
                                into {{COMPANY}}'s system</h5></center>
                    </div>
                    <div class="col-sm-2">
                        <div class="space-sm"></div>
                        <center><h1 class="light text-color2"><span class="text-color2">600</span></h1></center>
                        <div class="space-sm"></div>
                        <center><h4 class="semibold">Industries</h4></center>
                        <div class="space-xs"></div>
                        <center><h5 class="light">use {{COMPANY}} reviews
                                <br class="hidden-sm hidden-md hidden-lg"/>
                                to make decisions</h5></center>
                    </div>
                </div>

            </div>
            <div class="space-sm"></div>
            <div class="space-md"></div>
        </div>
        <div class="line-color2"></div>


        <div class="space-xl"></div>
        <div class="belt">
            <center><h1 class="normal">Stand Up To Harmful Clients <span class="bold">Everywhere.</span></h1></center>
            <div class="space-sm"></div>
            <center>
                <h2 class="extralight">
                    Join the thousands of businesses who already use Clientype<br class="hidden-xs"/>
                    reviews to make decisions on who to do business with.
                </h2>
            </center>
            <div class="space-md"></div>
            <center>
                <a href="{{URL::action("UserController@getRegister")}}">
                    <div class="pointer button-home button-home-color2">JOIN NOW!</div>
                </a>
            </center>
        </div>


        <div class="space-sm"></div>
        <div class="space-xl"></div>

        <!-- End Landing -->
    </div>

@stop
