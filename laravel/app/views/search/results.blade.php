<?php
/*
 * inbound $data [$customers, $messages]
 * access $customer, $messages, etc... directly
 * */
?>

<head>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400' rel='stylesheet' type='text/css'>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
    {{HTML::style("css/default.css")}}
    {{HTML::style("css/results.css")}}
</head>

<!-- nav band-->
<div class="results-wrapper">
    <div class="row">
        <div class="col-sm-offset-1 col-sm-10">

            <br/>
            <div class="panel panel-default">
                <div class="space-sm"></div>

                <div>
                    <div>
                        <div class="row">
                            <div class="col-sm-2 text-center"><label>Average</label></div>
                            <div class="col-sm-2"><label><!-- none --></label></div>
                            <div class="col-sm-2"><label>Name</label></div>
                            <div class="col-sm-5">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>Address</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label><!-- none --></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-1 hidden-xs"><label><!-- none --></label></div>
                        </div>
                    </div>
                </div>

                <div class="space-sm"></div>



                @foreach($customers as $customer)
                @include("result.result")
                @endforeach


            </div>
                 <nav>
                    <ul class="pagination">
                        <li>
                            <a href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li>
                            <a href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            <div class="space-sm"></div>
        </div> 
    </div>
</div>






