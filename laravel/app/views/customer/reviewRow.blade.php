

<div ng-repeat="review in CustCtrl.customer.reviews">
    <div class="space-sm"></div>
    <div class="row">
        <div class="col-sm-2">
            <div class="text-center review-user-icon">
                <br/>

                <div ng-if="review.user.vetted">
                    <div data-toggle="tooltip" title="Verified Account">
                        <i class="fa fa-user"></i>
                    </div>
                </div>
                <div ng-if="!review.user.vetted">
                    <div data-toggle="tooltip" title="Unverified Account">
                        <i class="fa fa-user nonvarified"></i>
                    </div>
                </div>

                <div class="review-date non-printable">@{{review.created_at | DateTime | date:'M-d-yyyy'}}</div>
            </div>
            <div class="space-sm"></div>
        </div>



        <div class="col-sm-3 list-group-band">
            <ul class="list-group">
                <li class="list-group-item">
                    <small>Payment</small>
                    <span class="badge badge-@{{CustCtrl.ratingToClass(review.pay)}}">
                        @{{review.pay}}
                    </span>
                </li>
                <li class="list-group-item">
                    <small>Working With</small>
                    <span class="badge badge-@{{CustCtrl.ratingToClass(review.ease)}}">
                        @{{review.ease}}
                    </span>
                </li>
                <li class="list-group-item">
                    <small>Kindness</small>
                    <span class="badge badge-@{{CustCtrl.ratingToClass(review.nice)}}">
                        @{{review.nice}}
                    </span>
                </li>
            </ul>
            <div class="space-sm"></div>
        </div>

        <div class=" col-sm-5">
            <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                @{{review.content}}
            </p>
            <div class="space-sm"></div>
        </div>

        <div class="col-sm-2">
            <div id="tooltip" data-toggle="tooltip" data-placement="top" title="Report this review">
            <span data-toggle="modal" data-target="#reportreview-target"
                  class="color-icon-light pull-right glyphicon glyphicon-flag"></span>
            </div>
            <br/>
            {{--TODO: implement the flags 1mike12--}}

            {{--@if("innapropriate review")--}}
            {{--<div class="pull-right"><span--}}
            {{--class="label label-danger">Inappropriate ({{"inapropriate #"}}--}}
            {{--)</span></div><br>--}}
            {{--@endif--}}

            {{--@if("innapropriate review")--}}
            {{--<div class="pull-right"><span--}}
            {{--class="label label-warning">Inaccurate ({{"innacurate #"}})</span>--}}
            {{--</div><br>--}}
            {{--@endif--}}

            {{--@if("innapropriate review")--}}
            {{--<div class="pull-right"><span class="label label-info">Other ({{"other#"}}--}}
            {{--)</span></div><br>--}}
            {{--@endif--}}

            {{--@if("innapropriate review")--}}
            {{--<div class="pull-right"><span class="label label-danger">Inappropriate</span>--}}
            {{--</div><br>--}}
            {{--@endif--}}

            {{--@if("innapropriate review")--}}
            {{--<div class="pull-right"><span class="label label-warning">Inaccurate</span>--}}
            {{--</div><br>--}}
            {{--@endif--}}

            {{--@if("innapropriate review")--}}
            {{--<div class="pull-right"><span class="label label-info">Other</span></div><br>--}}
            {{--@endif--}}
        </div>
    </div>
</div>