<?php
//inbound Eloquent $customer, mainly to get $customer->id
?>

<!-- =================== Only for printing =================== -->
<div class="row printable">
    <div class="col-xs-offset-3 col-xs-6">
        <center>
            <img src="{{URL::asset("images/logo-register.png")}}" alt="Error" class="logo-header"/>

            <br/>

            <div style="font-size: 9px;">
                All reviews found on {{COMPANY}}.com are opinions described
                in our Terms of use page. Note that this is a condensed copy
                of a review. This copy as well as any other copy of a review is
                full confidential and is meant only for personal use and should
                not be shared outside of business operations.

            </div>
        </center>
        <div class="space-md"></div>
    </div>
</div>
<!-- =================== Only for printing =================== -->


<div class="customer-wrapper" controller-init='{"customer_id":{{$customer->id}}}'
     ng-controller="CustomerController as CustCtrl">

        <div class="container-fluid">

            <div class="no-gutter non-printable">
                <div class="cu-house-band">
                <div class="row">

                    <div class="col-sm-4">
                        <div class="map-band">
                            <div id='streetViewCanvas' style='height: 60%; width: 100%;'></div>
                            <div id='mapCanvas' style='height: 40%; width: 100%;'></div>
                        </div>
                    </div>
                    @include("customer.googleMapScript")

                    <div class="col-sm-8">
                         <div class="belt">
                        <div class="space-md"></div>

                        <div class="row">
                            <div class="col-xs-8">
                                <h1> @{{CustCtrl.customer.last_name}}&nbsp;
                                    <small>
                                        @{{CustCtrl.customer.first1}}
                                        <i ng-if="customer.first2 !==''">&nbsp;/&nbsp;</i>
                                        @{{ CustCtrl.customer.first2 }}
                                    </small>
                                </h1>

                                <div class="address-box">
                                    <h3>
                                        @{{CustCtrl.customer.address1}}<br>
                                    <span ng-if="CustCtrl.customer.address2 !== ''">
                                        @{{CustCtrl.customer.address2}}<br>
                                    </span>
                                        @{{CustCtrl.customer.city}},&nbsp;@{{CustCtrl.customer.state}}
                                        &nbsp;@{{CustCtrl.customer.zip}}
                                    </h3>
                                </div>
                            </div>


                            <div class="col-sm-4 non-printable">
                                <center>
                                <button class="btn btn-primary button-customer" data-toggle="modal"
                                        data-target="#review-target">
                                    <span class="pull-left">Review Client</span>
        
                                        <i class="pull-right fa fa-star"></i>
                                </button>

                                <div class="space-xs"></div>

                                <button class="btn"
                                        ng-class="CustCtrl.customer.isMyCustomer ? 'btn-info button-customer' : 'btn-default button-customer'"
                                        ng-click="CustCtrl.toggleMyCustomer()">
                                    &nbsp;
                                    <span class="pull-left">@{{CustCtrl.toggleCustomerText}}</span>
                                    <i class="pull-right fa fa-tag"></i>
                                    &nbsp;
                                </button>
                                
                                       <div class="space-xs"></div>
                                       <button class="btn btn-default button-customer" data-toggle="modal" data-target="#reportclient-target">
                                           <span class="pull-left">Edit Information</span>
                                           <i class="pull-right fa fa-pencil-square-o"></i>
                                       </button>

                                <div class="space-xs"></div>

                                <!-- feeding customer id variable into model -->
                                <a href="javascript:window.print()">
                                    <button type="button" class="btn btn-default button-customer">
                                        <span class="pull-left">Print Page</span>
                                        <i class="pull-right fa fa-print"></i>
                                    </button>
                                </a>
                                 <div class="space-md"></div>
                                </center>
                            </div>
                        </div>

                            <div class="row">                                             
                                   <div class="col-sm-4">
                                <div class="customer-rating-avg">
                                  
                                        <center>
                                        <span class="font-size-xl ultrabold">
                                            @{{CustCtrl.customer.overallAvg}}
                                        </span>

                                            @if($customer->overallAvg>=3.0)
                                                <i class="fa fa-thumbs-o-up fa-5x text-color-success"></i>
                                            @else
                                                <i class="fa fa-thumbs-o-down fa-5x text-color-danger"></i>
                                            @endif

                                        </center>
                                    </div>
                                          </div>

                                    
                                <div class="col-sm-6">

                                        <!-- PAYMENT ====================================================== -->
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <h4>Payment</h4>
                                            </div>

                                            <div class="col-sm-1">
                                                <h4 class="ultrabold">@{{CustCtrl.customer.payAvg}}</h4>
                                            </div>

                                            <div class="col-sm-7">
                                                <div class="progress">
                                                    <div class="progress-bar progress-bar-@{{CustCtrl.ratingToClass(CustCtrl.customer.payAvg)}}"
                                                         role="progressbar"
                                                         style="width:@{{CustCtrl.ratingToPercent(CustCtrl.customer.payAvg)}}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- EASY ========================================================= -->
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <h4>Working With</h4>
                                            </div>

                                            <div class="col-sm-1">
                                                <h4 class="ultrabold">@{{CustCtrl.customer.easeAvg}}</h4>   
                                            </div>

                                            <div class="col-sm-7">
                                                <div class="progress">
                                                    <div class="progress-bar progress-bar-@{{CustCtrl.ratingToClass(CustCtrl.customer.easeAvg)}}"
                                                         role="progressbar"
                                                         style="width:@{{CustCtrl.ratingToPercent(CustCtrl.customer.easeAvg)}}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- NICE =========================================================== -->
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <h4>Kindness</h4>
                                            </div>

                                            <div class="col-sm-1">
                                                   <h4 class="ultrabold">@{{CustCtrl.customer.niceAvg}}</h4>
                                            </div>

                                            <div class="col-sm-7">
                                                <div class="progress">
                                                    <div class="progress-bar progress-bar-@{{CustCtrl.ratingToClass(CustCtrl.customer.niceAvg)}}"
                                                         role="progressbar"
                                                         style="width:@{{CustCtrl.ratingToPercent(CustCtrl.customer.niceAvg)}}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                
                                
                                    <!-- ================================================================ -->
                                    <div class="col-sm-2 text-right non-printable">
                                        {{--TODO: add they moved tags--}}
                                        {{--@if(true)--}}
                                        {{--<div class="pull-right">--}}
                                        {{--<span class="label label-default">They Moved ({{"moved #"}})</span>--}}
                                        {{--</div><br>--}}
                                        {{--@endif--}}

                                        {{--@if(true)--}}
                                        {{--<div class="pull-right"><span class="label label-default">They Moved</span>--}}
                                        {{--</div><br>--}}
                                        {{--@endif--}}

                                        {{--@if(true)--}}
                                        {{--<div class="pull-right"><span class="label label-info">Other ({{"other #"}}--}}
                                        {{--)</span>--}}
                                        {{--</div><br>--}}
                                        {{--@endif--}}

                                        {{--@if(true)--}}
                                        {{--<div class="pull-right"><span class="label label-info">Other</span>--}}
                                        {{--</div><br>--}}
                                        {{--@endif--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
      </div>


        <!-- Reviews -->
        <!-- ============================================================== -->
        <div class="comment-wrapper">
            <div class="sr">

                <div class="container-fluid">
                    <div class="space-sm"></div>
                    <div class="row">
                        <div class="col-md-offset-1 col-md-10">

                            <div class="panel panel-default">
                                <div class="panel-heading hidden-xs">
                                    <div class="row no-gutter-vertical">
                                        <label class="col-xs-2">Company</label>
                                        <label class="col-xs-3">Review</label>
                                        <label class="col-xs-5">Comment</label>
                                        <label class="col-xs-2"><!-- blank --></label>

                                    </div>
                                </div>
                                <div class="panel-body">
                                    @include("customer.reviewRow")
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="space-md"></div>
    </div>

    @include("modal.reportclient")
@include("modal.reportreview")