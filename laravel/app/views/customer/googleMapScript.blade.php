<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
<script>
    var address = "{{$customer->addressToString()}}";

    var geocoder = new google.maps.Geocoder();
    var streetViewService = new google.maps.StreetViewService();

    //phase 1
    geocoder.geocode({'address': address}, function (results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
            var houseLatLng = results[0].geometry.location;

            //phase 2
            streetViewService.getPanoramaByLocation(houseLatLng, 50, function (data, status) {

                if (status === google.maps.StreetViewStatus.OK) {
                    //=============PANORAMA===============
                    var panoLatLng = data.location.latLng;
                    var heading = google.maps.geometry.spherical.computeHeading(panoLatLng, houseLatLng);


                    var panoramaOptions = {
                        visible: true,//visible on load (dont have to do panorama.setVisible(true))
                        imageDateControl: true,
                        addressControl: false,
                        position: panoLatLng,
                        pov: {
                            heading: heading,
                            pitch: 0
                        }
                    };

                    //makes panorama on Element ID "#streetViewCanvas"
                    var panorama = new google.maps.StreetViewPanorama(document.getElementById("streetViewCanvas"), panoramaOptions);


                    //===============MAP====================
                    //https://developers.google.com/maps/documentation/javascript/controls#DefaultControls
                    var mapOptions = {
                        center: houseLatLng,
                        zoom: 14,
                        //reset all MAP UI 
                        disableDefaultUI: true,
                        mapTypeControl: true
                    };
                    //makes map on element with ID "#mapCanvas"
                    var map = new google.maps.Map(document.getElementById("mapCanvas"),
                            mapOptions);
                    var marker = new google.maps.Marker({
                        position: houseLatLng,
                        map: map,
                        animation: google.maps.Animation.DROP
                    });
                } else {
                    //TODO 1mike12 no response from google apis
                }
            });

        } else if (status === google.maps.GeocoderStatus.ZERO_RESULTS) {
            console.log("No address matched. Status: " + status);
        } else {
            console.log("Geocode was not successful for the following reason: " + status);
        }
    });
</script>

