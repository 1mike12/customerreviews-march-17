
@extends("layouts.default")
@section("content")

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-offset-2 col-sm-8">
            <div class="addCustomer-wrapper">
                <div class="space-md"></div>
                <div class="belt">  
                    <h3 class="bold">Add Client</h3>
                    <div class="space-sm"></div>

                    <div class="row">
                        <div class="col-sm-6">
                            <label>First Name</label>
                            <input type="text" class="form-control" placeholder="Smith">
                            <div class="space-sm"></div>

                            <label>Last Name</label>
                            <input type="text" class="form-control" placeholder="John">
                            <div class="space-sm"></div>

                            <label>Address</label>
                            <input type="text" class="form-control" placeholder="23 Main Street">
                            <div class="space-sm"></div>

                            <label>Address 2</label>
                            <input type="text" class="form-control" placeholder="PO Box 182">
                            <div class="space-sm"></div>

                            <label>City</label>
                            <input type="text" class="form-control" placeholder="Boston">
                            <div class="space-sm"></div>

                            <label>State</label>
                            <input type="text" class="form-control" placeholder="Massatusets">
                            <div class="space-sm"></div>

                            <label>Zip Code</label>
                            <input type="text" class="form-control" placeholder="02207">
                            <div class="space-sm"></div>

                            <label>Phone Number <small>(optional)</small></label>
                            <input type="text" class="form-control" placeholder="(978) 329-9384">
                            <div class="space-sm"></div>


                        </div>
                        <div class="col-sm-6">

                            <div class="addCustomer-map">
                                <div id="map-addCustomer"></div>
                            </div>

                        </div>

                    </div>
                </div>
                
                 <div class="space-md"></div>
                 
                 <center>
                     <a class="#"><div class="btn btn-lg btn-primary">Submit</div></a>
                 </center>
                 
                 <div class="space-md"></div>
            </div>
        </div>
    </div>
    
</div>

 <div class="space-md"></div>
 

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>
<script>
    function initialize() {
        var myLatlng = new google.maps.LatLng(42.78856, -71.20089);
        var mapOptions = {
            zoom: 15,
            center: myLatlng
        }
        var map = new google.maps.Map(document.getElementById('map-addCustomer'), mapOptions);

        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: 'Map'
        });
    }

    google.maps.event.addDomListener(window, 'load', initialize);

</script>

@stop
