

<!-- Incoming customer id variable -->  
<!-- Modal -->
<div class="modal" id="review-target" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button style="font-size: 40px"type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h3 class="modal-title" id="myModalLabel">

                   Review: "client name"

                    @{{CustCtrl.customer.last_name}}&nbsp;
                        <small>
                            @{{CustCtrl.customer.first1}}
                            <i ng-if="customer.first2 !==''">&nbsp;/&nbsp;</i>
                            @{{ CustCtrl.customer.first2 }}
                        </small>

                </h3>
            </div>
            <div class="modal-body belt">

                <br/>
                <!-- Review -->  
                <!-- ================================================================ -->

                <div class="five-review row">
                    <div class="col-sm-4">
                        <span>Payment</span>
                    </div>

                    <div class="col-sm-6">
                        <div class="panel panel-default">
                            <div class="panel-body text-center">
                             
                                <div id="example-1"></div>
                                <span>&nbsp;&nbsp;&nbsp;</span><span id="example-rating-1"></span>
                         
                            </div>             
                        </div>                   
                    </div>
                </div>



                <!-- ================================================================ -->
                <div class="five-review row">
                    <div class="col-sm-4">
                        <span>Working With</span>
                    </div>

                    <div class="col-sm-6">
                        <div class="panel panel-default">
                            <div class="panel-body text-center">
                                <div id="example-2"></div>
                                <span>&nbsp;&nbsp;&nbsp;</span><span id="example-rating-2"></span>
                            </div>                
                        </div>                   
                    </div> 
                </div>

                <!-- ================================================================ -->
                <div class="five-review row">
                    <div class="col-sm-4">
                        <span>Kindness</span>
                    </div>

                    <div class="col-sm-6">
                        <div class="panel panel-default">
                            <div class="panel-body text-center">
                                <div id="example-3"></div>
                                <span>&nbsp;&nbsp;&nbsp;</span><span id="example-rating-3"></span>
                            </div>                
                        </div>                   
                    </div>
                </div>

                <!-- ================================================================ -->
                <br/>
                <div class="row">
                    <div class="col-sm-4">
                        <span>Experience</span>
                    </div>

                    <div class="col-sm-8">
                        <textarea class="form-control input-lg" placeholder="Describe the experience" rows="5" maxlength="500"></textarea>
                        <span><small>*minimum of 20 words</small>

                    </div>
                </div>




                <!-- review tags -->

                <div class="space-md"></div>
                <div class="row">
                    <div class="col-sm-offset-4">
                        <div class="review-tag-wrapper">
                            <label>Good</label>   
                            <br/>
                            <div class="noselect">
                                <a class="pointer active-tag-pro"><span class="text-center">TIPS WELL</span></a>
                                <a class="pointer active-tag-pro"><span class="text-center">LATE PAYMENT</span></a>
                                <a class="pointer active-tag-pro"><span class="text-center">EASY GOING</span></a>
                                <a class="pointer active-tag-pro"><span class="text-center">PAYS ON TIME</span></a>
                                <a class="pointer active-tag-pro"><span class="text-center">ACTIVE TAG</span></a>
                                <a class="pointer active-tag-pro"><span class="text-center">ACTIVE TAG</span></a>
                                <a class="pointer active-tag-pro"><span class="text-center">ACTIVE TAG</span></a>
                                <a class="pointer active-tag-pro"><span class="text-center">ACTIVE TAG</span></a>
                                <a class="pointer active-tag-pro"><span class="text-center">ACTIVE TAG</span></a>
                                <a class="pointer active-tag-pro"><span class="text-center">ACTIVE TAG</span></a>
                                <a class="pointer active-tag-pro"><span class="text-center">ACTIVE TAG</span></a>
                                <a class="pointer active-tag-pro"><span class="text-center">ACTIVE TAG</span></a>
                                <a class="pointer active-tag-pro"><span class="text-center">ACTIVE TAG</span></a>
                                <a class="pointer active-tag-pro"><span class="text-center">ACTIVE TAG</span></a>
                                <a class="pointer active-tag-pro"><span class="text-center">ACTIVE TAG</span></a>

                            </div>
                            <div class="space-md"></div>
                            <label>Bad</label>
                            <br/>
                            <div class="noselect">
                                <a class="pointer active-tag-con"><span class="text-center">LATE PAYMENT</span></a>
                                <a class="pointer active-tag-con"><span class="text-center">NO PAYMENT</span></a>
                                <a class="pointer active-tag-con"><span class="text-center">RUDE</span></a>
                                <a class="pointer active-tag-con"><span class="text-center">BAD COMMUNICATOR</span></a>
                                <a class="pointer active-tag-con"><span class="text-center">WORST CLIENT</span></a>
                                <a class="pointer active-tag-con"><span class="text-center">MEAN</span></a>
                                <a class="pointer active-tag-con"><span class="text-center">ARGUMENTATIVE</span></a>
                                <a class="pointer active-tag-con"><span class="text-center">HOVERER</span></a>
                                <a class="pointer active-tag-con"><span class="text-center">LOW BALLER</span></a>
                                <a class="pointer active-tag-con"><span class="text-center">ACTIVE TAG</span></a>
                                <a class="pointer active-tag-con"><span class="text-center">ACTIVE TAG</span></a>
                                <a class="pointer active-tag-con"><span class="text-center">ACTIVE TAG</span></a>
                                <a class="pointer active-tag-con"><span class="text-center">ACTIVE TAG</span></a>
                            </div>
                        </div>
                    </div>
                </div>       

                <div class="space-lg"></div>

                <!-- Review -->  
            </div>
            <div class="modal-footer">
          
                <button type="button" class="btn btn-lg btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-lg btn-info">Submit</button>
            </div>
        </div>
    </div>
</div>