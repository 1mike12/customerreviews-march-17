
<!-- Modal -->
<div class="modal fade" id="reportclient-target" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Report Client - {{$customer->last}}&nbsp;<small>{{$customer->first1}}/&nbsp;{{$customer->first2}}</small></h4>
            </div>
            <div class="belt modal-body">
                <div class="row">

                    <div class="alert alert-info" role="alert"><strong>Reporting a client</strong> will immediately <a href="#">tag</a> the review, warning others of the
                        possible issue. All reports will be investigated by the {{COMPANY}} team. Please report only those clients with issues in which you have a high level
                        of confidence with. Thank you for helping to improve reviews accuracy.</div>
                </div>

                <form role="form">
                    <div class="report-tag">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="pull-left">Client in no longer living here</div>
                                <a><div class="pull-right"><span class="label label-default">Client Moved</span></div></a>
                            </div>
                        </div>

                        <br>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="pull-left">Other <small>(other reason for investigation)</small></div>
                                <a><div class="pull-right"><span class="label label-info">Other</span></div></a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </div>
</div>
