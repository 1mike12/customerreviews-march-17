
<div class="signin-wrapper">
    <!-- Incoming customer id variable -->  
    <!-- Modal -->
    <div id="login-target" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="false">
        <div class="modal-dialog modal-sm">
            <div class="modal-content modal-content-home">

                {{ Form::open(["action"=>"UserController@postSignin", 'class'=>'form']) }}

                <center>

                    <div class="signin-box">  
                        <div class="form1 form-group">
                            {{ Form::text('email', null, array('class'=>'form-control', 'placeholder'=>'Email')) }}
                        </div>

                        <div class="form1 form-group">
                            {{ Form::password('password', array('class'=>'form-control', 'placeholder'=>'Password')) }}
                        </div>

                        <div class="form2 form-group">
                            {{ Form::submit('Log in', array('class'=>'btn btn-primary btn-block'))}}

                        </div>

                        <div class="container-fluid">

                            <div class="pull-left">
                                <input id="checkbox-1" type="checkbox" />
                                <label for="checkbox-1">Remember me</label>
                            </div> 
                            <a class="pull-right" href="#"><span>Forget password?</span></a>

                        </div>
 </div>
                    
                        <div class="container-fluid">
                            <hr class="col-xs-10"/>
                        </div>

                        <small>Don't have an account?&nbsp;&nbsp;&nbsp;</small><a href="{{URL::action("UserController@getRegister")}}">Sign up</a> 

                </center>

                {{ Form::close()}}

            </div>
        </div>
    </div>
</div>
