
<!-- Register Login -->
<!-- ============================================================ -->
<div id="signin-wrapper">
    {{ Form::open(["action"=>"UserController@postSignin", 'class'=>'form']) }}
    <center>
        <center><h4>Login to Your {{COMPANY}} Account</h4></center>
        <div class="space-sm"></div>
         <div class="space-sm"></div>
        <div class="signin-box register-login">
            <div class="form1 form-group">
                {{ Form::text('email', null, array('class'=>'form-control', 'placeholder'=>'Email')) }}
            </div>
            <div class="form1 form-group">
                {{ Form::password('password', array('class'=>'form-control', 'placeholder'=>'Password')) }}
            </div>
            <div class="form2 form-group">
                {{ Form::submit('Login', array('class'=>'btn btn-primary btn-block'))}}
            </div>
        </div>
    </center>
    {{ Form::close()}}
</div>

<div class="register-register-text pull-left">
    <small>Don't have an account?
        <a href="#r1" aria-controls="4" role="tab" data-toggle="tab">Get Started</a>
        <br/>
        <a class="pull-left" href="#"><span>Forget password?</span></a>
    </small>
</div> 