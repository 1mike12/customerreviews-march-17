
<!-- Register Limited -->
<!-- ============================================================ -->
<div class="row">
    <div class="col-sm-12">
       
        <div ng-app="register" ng-controller="RegisterController as rCtrl">
            {{ Form::open(["action"=>"UserController@postRegister", 'class'=>'form form-horizontal']) }}
            <div class="form-group <?php echo ($errors->has("company_name") ? "has-error" : ($submitted ? "has-success" : "")) ?>">
                {{ Form::text('company_name', null, array('class'=>'form-control', 'placeholder'=>'Business Name')) }}
                <div class="col-sm-12">
                    <span class="help-block">
                        @if ($errors->has("company_name"))
                        {{$errors->first("company_name")}}
                        @else
                        @endif
                    </span>
                </div>
            </div>

      
            <select class="form-control">
                <option>General industry</option>
                <option>Construction/Instalation/Building</option>
                <option>Maintenance/Repair</option>
                <option>Teacher/Mentor/Instructor</option>
                <option>Producer/</option>
                <option>Freelance</option> 
                <option>Other</option>
            </select>
            
             <div class="col-sm-12">
                    <span class="help-block">
                        @if ($errors->has("company_name"))
                        {{$errors->first("company_name")}}
                        @else
                        @endif
                    </span>
                </div>
         

            <div class="form-group {{($errors->has("domain") ? "has-error" : ($submitted ?"has-success": ""))}}">
                <div class="input-group">
                    <div class="input-group-addon">www.</div>
                    <input type="text" name="domain" class="form-control" placeholder="Domain" ng-model="domain" ng-init="domain = '{{Input::old("domain")}}'" value="{{Input::old("domain")}}" style="padding-left:5px;"/>
                </div>
                <div class="col-sm-12">
                    <span class="help-block">
                        @if ($errors->has("domain"))
                        {{$errors->first("domain")}}
                        @else
                        @endif
                    </span>
                </div>
            </div>

            <div class="form-group {{($errors->has("address1") ? "has-error" : ($submitted ?"has-success": ""))}}">
                {{ Form::text('address1', null, array('class'=>'form-control', 'placeholder'=>'Address')) }}
                <div class="col-sm-12">
                    <span class="help-block">
                        {{$errors->first("address1")}}
                    </span>
                </div>
            </div>

            <div class="form-group {{($errors->has("address2") ? "has-error" : ($submitted ?"has-success": ""))}}">
                {{ Form::text('address2', null, array('class'=>'form-control', 'placeholder'=>'Address 2')) }}

                <div class="col-sm-12">
                    <span class="help-block">
                        {{$errors->first("address2")}}
                    </span>
                </div>
            </div>

            <div class="form-group {{($errors->has("city") ? "has-error" : ($submitted ?"has-success": ""))}}">
                {{ Form::text('city', null, array('class'=>'form-control', 'placeholder'=>'City')) }}
                <div class="col-sm-12">
                    <span class="help-block">
                        {{$errors->first("city")}}
                    </span>
                </div>
            </div>

            <div class="form-group">
                <div class="no-gutter-left">
                <div class="col-xs-8 {{($errors->has("state") ? "has-error" : ($submitted ?"has-success": ""))}}">
                    {{Form::states("state", Input::old("state"), ["class"=>"form-control"])}}
                </div>
                    </div>
                <div class="no-gutter-right">
                <div class="col-xs-4 {{($errors-> has("zip") ? "has-error" : ($submitted ?"has-success": ""))}}">
                    {{ Form::text('zip', null, array('class'=>'form-control', 'placeholder'=>'Zip')) }}
                </div>
                     </div>
                <div class="col-xs-12">
                    <span class="help-block">
                        {{$errors->first("state")}}
                        {{$errors->first("zip")}}
                    </span>
                </div>
            </div>

            <div class="form-group {{($errors->has("phone") ? "has-error" : ($submitted ?"has-success": ""))}}">
                {{ Form::text('phone', null, array('class'=>'form-control', 'placeholder'=>'Phone')) }}
                <div class="col-sm-12">
                    <span class="help-block">
                        @if ($errors->has("phone"))
                        {{$errors->first("phone")}}
                        @else
                        @endif
                    </span>
                </div>
            </div>

            <div class="form-group {{($errors->has("email") ? "has-error" : ($submitted ?"has-success": ""))}}">
                <div class="input-group">
                    {{ Form::text('email', null, array('class'=>'form-control', 'placeholder'=>'Email Address', "style"=>"text-align:right; padding-right:5px;")) }}
                    <div class="input-group-addon">@[[domain]]</div>
                </div>
                <div class="col-sm-12">
                    <span class="help-block">
                        @if ($errors->has("email"))
                        {{$errors->first("email")}}
                        @else
                        @endif
                    </span>
                </div>
            </div>

            <div class="form-group {{($errors->has("password") ? "has-error" : ($submitted ?"has-success": ""))}}">
                {{ Form::password('password', array('class'=>'form-control', 'placeholder'=>'Password')) }}
                <div class="col-sm-12">
                    <span class="help-block">
                        {{$errors->first("password")}}
                    </span>
                </div>
            </div>

            <div class="form-group {{($errors->has("password_confirmation") ? "has-error" : ($submitted ?"has-success": ""))}}">
                {{ Form::password('password_confirmation', array('class'=>'form-control', 'placeholder'=>'Confirm Password')) }}
                <div class="col-sm-12">
                    <span class="help-block">
                        {{$errors->first("password_confirmation")}}
                    </span>
                </div>
            </div>

            <div class="form-group {{($errors->has("terms") ? "has-error" : ($submitted ?"has-success": ""))}}">
                <div class="form-group">
                    {{ Form::submit('Sign Up Free', array('class'=>'btn btn-lg btn-primary btn-block'))}}
                </div>
                {{ Form::close() }}
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <center>
                    <h5>
                        <small>                                  
                            By clicking "Sign Up Free" I agree
                            to {{COMPANY}} <a href="{{URL::to("terms")}}">Terms of Service.</a>
                        </small>
                    </h5>
                </center>
            </div>
        </div>

        <div class="register-login-text">
            <small>Already have an account?
                <a href="#register-login" aria-controls="4" role="tab" data-toggle="tab">Login!</a>
            </small>
        </div> 
    </div>
</div>