
@extends("layouts.default")
@section("content")

<div class="settings-wrapper">
   <div class="space-md"></div>
    <div class="container-fluid">
        <div class="col-sm-offset-2 col-sm-8">

            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                <div class="panel">
                    <hr/>
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="false" aria-controls="collapse1">
                        <div class="row panel-heading" role="tab" id="headingOne">
                            <div class="bold col-xs-4">Business Name</div><div class="col-xs-4">Pide Piper LLC.</div><div class="col-xs-4 text-right"><i class="fa fa-pencil">&nbsp;</i>Edit</div>

                        </div>
                    </a>

                    <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <div class="well container-fluid">
                                <!-- well content -->
                                <label>Change Name</label>
                                <input type="text" id="inputHelpBlock" class="form-control" aria-describedby="helpBlock" placeholder="New name">

                                <span id="helpBlock" class="help-block">Your are limited to the frequency and number of times you change your businesses name.</span>
                                <input class="pull-right btn btn-default" type="submit" value="Submit">
                            </div>
                        </div>
                    </div>
                </div>
<hr/>
                <div class="panel">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
                        <div class="row panel-heading" role="tab" id="headingOne">
                            <div class="bold col-xs-4">Industry</div><div class="col-xs-4">File sharing</div><div class="col-xs-4 text-right"><i class="fa fa-pencil">&nbsp;</i>Edit</div>

                        </div>
                    </a>

                    <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <div class="well container-fluid">
                                <!-- well content -->
                                <label>Change Industry</label>
                                <input type="text" id="inputHelpBlock" class="form-control" aria-describedby="helpBlock" placeholder="New industry">

                                <span id="helpBlock" class="help-block">Your are limited to the the number of times you can change your industry.</span>
                                <input class="pull-right btn btn-default" type="submit" value="Submit">
                            </div>
                        </div>
                    </div>
                </div>
<hr/>
                <div class="panel">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
                        <div class="row panel-heading" role="tab" id="headingOne">
                            <div class="bold col-xs-4">Email</div><div class="col-xs-4">pidepiper@email.com</div><div class="col-xs-4 text-right"><i class="fa fa-pencil">&nbsp;</i>Edit</div>

                        </div>
                    </a>

                    <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <div class="well container-fluid">
                                <!-- well content -->
                                <label>Change Primary Email</label>
                                <input type="text" id="inputHelpBlock" class="form-control" aria-describedby="helpBlock" placeholder="New email">

                                <span id="helpBlock" class="help-block">Your can not change your primary email</span>
                                <input class="pull-right btn btn-default" type="submit" value="Submit">
                            </div>
                        </div>
                    </div>
                </div>
<hr/>
                <div class="panel">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                        <div class="row panel-heading" role="tab" id="headingOne">
                            <div class="bold col-xs-4">Password</div><div class="col-xs-4">*******</div><div class="col-xs-4 text-right"><i class="fa fa-pencil">&nbsp;</i>Edit</div>

                        </div>
                    </a>

                    <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <div class="well container-fluid">
                                <!-- well content -->
                                <label>Change Password</label>
                                <input type="text" id="inputHelpBlock" class="form-control" aria-describedby="helpBlock" placeholder="New password">
                                <div class="space-xs"></div>
                                <input type="text" id="inputHelpBlock" class="form-control" aria-describedby="helpBlock" placeholder="Re-type password">

                                <span id="helpBlock" class="help-block">Change your password</span>
                                <input class="pull-right btn btn-default" type="submit" value="Submit">
                            </div>
                        </div>
                    </div>
                </div>
<hr/>
                <div class="panel">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" aria-controls="collapse5">
                        <div class="row panel-heading" role="tab" id="headingOne">
                            <div class="bold col-xs-4">Language</div><div class="col-xs-4">English (US)</div><div class="col-xs-4 text-right"><i class="fa fa-pencil">&nbsp;</i>Edit</div>

                        </div>
                    </a>

                    <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <div class="well container-fluid">
                                <!-- well content -->
                                <form>
                                    <fieldset disabled>
                                        <div class="form-group">
                                            <label for="disabledTextInput">Change Language</label>
                                            <input type="text" id="disabledTextInput" class="form-control" placeholder="-">

                                            <span id="helpBlock" class="help-block">Not available</span>
                                        </div>

                                        <input class="pull-right btn btn-default" type="submit" value="Submit">
                                    </fieldset>
                                </form> 
                            </div>
                        </div>
                    </div>
                </div>
<hr/>
                <div class="panel">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="false" aria-controls="collapse6">
                        <div class="row panel-heading" role="tab" id="headingOne">
                            <div class="bold col-xs-4">Verify Account</div><div class="col-xs-4">Verified</div><div class="col-xs-4 text-right"><i class="fa fa-pencil">&nbsp;</i>Edit</div>

                        </div>
                    </a>

                    <div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <!-- well content -->
                            <div class="alert alert-success" role="alert">Your account is verified<span class="pull-right glyphicon glyphicon-ok"></div>
                        </div>
                    </div>
                </div>
      <hr/>          
                 <div class="panel">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse7" aria-expanded="false" aria-controls="collapse7">
                        <div class="row panel-heading" role="tab" id="headingOne">
                            <div class="bold col-xs-4">Premium Account</div><div class="col-xs-4">Premium Account</div><div class="col-xs-4 text-right"><i class="fa fa-pencil">&nbsp;</i>Edit</div>

                        </div>
                    </a>

                    <div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <!-- well content -->
                            <div class="alert alert-info" role="alert">You have a Premium Account Free for life! This is our thanks for being an early adopter. <a href="#">Learn More</a>
                            <span class="pull-right glyphicon glyphicon-ok"></div>
                        </div>
                    </div>
                </div>
                <hr/>
                  <div class="panel">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse8" aria-expanded="false" aria-controls="collapse8">
                        <div class="row panel-heading" role="tab" id="headingOne">
                            <div class="bold col-xs-4">Membership Status</div><div class="col-xs-4">Not A Gold Member</div><div class="col-xs-4 text-right"><i class="fa fa-pencil">&nbsp;</i>Edit</div>

                        </div>
                    </a>

                    <div id="collapse8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <!-- well content -->
                            <div class="alert alert-warning" role="alert">You must review 20 clients for upcoming premium features, free for life
                            <i class="pull-right fa fa-info-circle"></i></div>
                        </div>
                    </div>
                </div>
                <hr/>
            </div>
             
        </div>
    </div>
    <div class="space-lg"></div>
    <div class="space-lg"></div>
</div>
@stop