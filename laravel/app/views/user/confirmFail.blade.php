<h1>Confirmation unsuccesful</h1>
<p>Something went wrong. Please check that you have followed the correct link</p>
<p>enter your email to resend confirmation message</p>
{{form::open(["action"=>"UserController@postResend", "class"=>"form-inline", "files"=>false])}}
{{form::text("email", null, ['class'=>'form-control', 'placeholder'=>'email'])}}
{{form::submit(null,["class"=>"btn btn-primary"])}}
{{form::close()}}