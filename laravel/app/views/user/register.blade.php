@extends("layouts.noHeadernoFooter")

<?php
if (Session::get("submitted")) {
    $submitted = true;
} else {
    $submitted = false;
}
?>

@section("content")
    <div class="register-wrapper"></div>
    <div class="drop-shadow"></div>
    <a href="{{URL::to("/")}}">
        <i class="fa fa-reply fa-2x"></i>
    </a>

    <div class="container-fluid">
        <div class="row">
            <center>
                <div class="col-sm-12">
                    <div class="register-modal">
                        <div class="register-modal-header">
                            <center>
                                <a href="{{URL::action("UserController@getRegister")}}">
                                    <img class="register-logo" src="{{URL::asset("images/logo-register.png")}}"
                                         alt="Error">
                                </a>
                            </center>

                        </div>
                        <div class="register-modal-body">
                            <div role="tabpanel">
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane" id="r1">

                                        <div role="tablist">
                                            <div class="register-modal-subheader">
                                                <center><h4>Get Started Absolutely <b class="text-color1">FREE.</b></h4>
                                                </center>
                                                <center>
                                                    <h5>
                                                        <small>All accounts are anonymous.</small>
                                                    </h5>
                                                </center>
                                            </div>

                                            <div role="presentation"><a id="limited-register-alert"
                                                                        href="#premium-register" aria-controls="1"
                                                                        role="tab" data-toggle="tab">
                                                    <div class="register-button register-button-premium">Premium
                                                        Account
                                                    </div>
                                                </a></div>

                                            <div class="space-sm"></div>
                                            <hr/>
                                            <div class="space-sm"></div>

                                            <div role="presentation"><a href="#domain-register" aria-controls="2"
                                                                        role="tab" data-toggle="tab">
                                                    <div class="register-button register-button-domain">Domain SignUp
                                                    </div>
                                                </a></div>

                                            <div class="space-sm"></div>

                                            <div role="presentation"><a href="#facbeook-register" aria-controls="3"
                                                                        role="tab" data-toggle="tab">
                                                    <div class="register-button register-button-facebook">Facebook
                                                        SignUp
                                                    </div>
                                                </a></div>

                                            <div class="space-sm"></div>

                                            <div role="presentation"><a id="limited-register-alert"
                                                                        href="#limited-register" aria-controls="4"
                                                                        role="tab" data-toggle="tab">
                                                    <div class="register-button register-button-limited">Limited
                                                        SignUp
                                                    </div>
                                                </a></div>

                                            <div class="register-login-text">
                                                <small>Already have an account?
                                                    <a href="#register-login" aria-controls="4" role="tab"
                                                       data-toggle="tab">Login!</a>
                                                </small>
                                            </div>
                                        </div>

                                    </div>
                                    <div role="tabpanel" class="tab-pane active" id="premium-register">
                                        @include("user.register-premium")
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="domain-register">
                                        @include("user.register-domain")
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="facbeook-register">
                                        @include("user.register-facebook")
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="limited-register">
                                        @include("user.register-limited")
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="register-login">
                                        @include("user.register-login")
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="space-lg"></div>
                </div>
            </center>
        </div>
    </div>
@stop







