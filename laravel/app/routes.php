<?php
Route::get("phpinfo", function() {
    return View::make("phpinfo");
});

Route::group(['prefix' => 'api', 'before' => 'auth'], function() {
    //keep underscore to be consistent in URL & angular (doesn't allow dash, my-customers for instance)
    Route::resource("my_customers", "MyCustomersController");
    Route::resource("recent_customers", "RecentCustomersController");
    Route::resource("best_customers", "BestCustomersController");
    Route::resource("worst_customers", "WorstCustomersController");
    Route::resource("customer", "api_CustomerController");
});

Route::get("settings", function() {
    return View::make("user.settings");
});

Route::get("data", function() {
    return View::make("layouts.data");
});

Route::get("addCustomer", function() {
    return View::make("customer.addCustomer");
});

Route::get("founders", function() {
    return View::make("home.founders");
});
//TODO - change this to actually be real 404 response
Route::get("404", function() {
    return View::make("errors.404");
});

Route::get("testing", function() {
    return View::make("home.testing");
});


//Dashboard shit not open to public===============
Route::group(["before" => "auth"], function() {
    Route::controller("dash", "DashController");
    Route::get("search", "SearchController@getSearch");
    Route::get("customer/{id?}", "CustomerController@getCustomer");
});
//user routes ====================================
Route::controller("user", "UserController");

//Datbase delete before going live=================
Route::controller("database", "DatabaseController");

Route::get("testform", function() {
    return View::make("testForm");
});

//homepage links====================================

Route::get("learncenter", function() {
    return View::make("learncenter.learncenter");
});


Route::get("press", function() {
    return View::make("home.press");
});

Route::get("acknowledgments", function() {
    return View::make("home.acknowledgments");
});

Route::get("testimonials", function() {
    return View::make("home.testimonials");
});

Route::get("terms", function() {
    return View::make("home.terms");
});

Route::get("register-premium", function() {
    return View::make("user.register-premium");
});

Route::get("blog", function() {
    return View::make("home.blog");
});

Route::get("privacy", function() {
    return View::make("home.privacy");
});

Route::get("contact", function() {
    return View::make("home.contact");
});

Route::get("about", function() {
    return View::make("home.about");
});

Route::get("faq", function() {
    return View::make("home.faq");
});


Route::get("/", function() {
    if (Auth::check()) {
        return Redirect::to("dash");
    } else {
        return View::make("home.home");
    }
});



/*
 * notes
 * config environment - bootstrap/start.php
 * config folders- app/config/live - local etc
 * load in extra stuff - start/global.php
 * 
 * can't have search@getIndex(parameters), because search/query thinks "query" is search@query
 */
